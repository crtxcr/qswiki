#include "pagelistrenderer.h"
#include "grouper.h"

PageListRenderer::PageListRenderer(Template &templ, UrlProvider &provider, Database &database)
{
	this->templ = &templ;
	this->urlProvider = &provider;
	this->database = &database;
}

std::string PageListRenderer::render(const std::vector<Page> &pagelist, std::string type) const
{
	TemplatePage pagelistrendergroup = this->templ->loadResolvedPart("pagelistrender_group");
	TemplatePage pagelistlink = this->templ->loadResolvedPart("pagelistrender_link");


	std::function<bool(const std::string &, const std::string &)> lesser = [](const std::string &a, const std::string &b) -> bool {
		return a < b;
	};

	std::function<bool(const std::string &, const std::string &)> greater = [](const std::string &a, const std::string &b) -> bool {
		return a > b;
	};

	using Grouper = Grouper<std::string, Page, std::function<bool(const std::string &,const std::string &)>>;

	Grouper grouper = (type == "letter") ? Grouper(lesser) : Grouper(greater);
	if(type == "letter")
	{
		auto az = [&](const Page &p) -> std::string {
			int upper = toupper(p.title[0]); // TODO: this is not unicode safe.
			return { (char)upper };
		};
		grouper.group(az, pagelist);
	}
	else
	{
		auto revisionDao = this->database->createRevisionDao();
		auto creationdate = [&revisionDao](const Page &p) -> std::string {
			return utils::formatLocalDate(revisionDao->getRevisionForPage(p.name, 1).value().timestamp, "%Y-%m");
		};
		grouper.group(creationdate, pagelist);
	}

	std::stringstream stream;
	std::string lastGroup = "";

	for(auto &entry : grouper.getResults())
	{
		std::string groupname = entry.first;
		const std::vector<const Page*> &pages = entry.second;
		if(lastGroup != groupname)
		{
			lastGroup = groupname;
			pagelistrendergroup.setVar("groupname", groupname);
			stream << pagelistrendergroup.render();
		}
		for(const Page *p : pages)
		{
			pagelistlink.setVar("inner", p->title);
			pagelistlink.setVar("href", this->urlProvider->page(p->name));
			stream << pagelistlink.render();
		}
	}
	return stream.str();
}
