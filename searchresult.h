#ifndef SEARCHRESULT_H
#define SEARCHRESULT_H
#include <string>
class SearchResult
{
  public:
	std::string query;
	std::string pagename;
};
#endif // SEARCHRESULT_H
