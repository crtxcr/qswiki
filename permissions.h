#ifndef PERMISSIONS_H
#define PERMISSIONS_H

#define PERM_CAN_NOTHING 0
#define PERM_CAN_READ 1 << 0
#define PERM_CAN_EDIT 1 << 1
#define PERM_CAN_PAGE_HISTORY 1 << 2
#define PERM_CAN_GLOBAL_HISTORY 1 << 3
#define PERM_CAN_DELETE 1 << 4
#define PERM_CAN_SEE_PAGE_LIST 1 << 5
#define PERM_CAN_CREATE 1 << 6
#define PERM_CAN_SEE_CATEGORY_LIST 1 << 7
#define PERM_CAN_SEE_LINKS_HERE 1 << 8
#define PERM_CAN_SEARCH 1 << 9
#define PERM_CAN_SET_PAGE_PERMS 1 << 10
#define PERM_IS_ADMIN (1L<<31)-1

#include <string>
#include <map>

class Permissions

{
  private:
	int permissions = 0;

  public:
	Permissions()
	{
		this->permissions = 0;
	}
	Permissions(int permissions);
	Permissions(const std::string &str);
	Permissions(Permissions &&o)
	{
		this->permissions = o.permissions;
	}

	Permissions(const Permissions &o)
	{
		this->permissions = o.permissions;
	}
	Permissions &operator=(const Permissions &o)
	{
		this->permissions = o.permissions;
		return *this;
	}

	Permissions &operator=(Permissions &&o)
	{
		this->permissions = o.permissions;
		return *this;
	}

	int getPermissions() const
	{
		return this->permissions;
	}

	bool canNothing() const
	{
		return this->permissions == PERM_CAN_NOTHING;
	}

	bool canRead() const
	{
		return this->permissions & PERM_CAN_READ;
	}

	bool canEdit() const
	{
		return this->permissions & PERM_CAN_EDIT;
	}
	bool canSeePageHistory() const
	{
		return this->permissions & PERM_CAN_PAGE_HISTORY;
	}
	bool canSeeGlobalHistory() const
	{
		return this->permissions & PERM_CAN_GLOBAL_HISTORY;
	}
	bool canCreate() const
	{
		return this->permissions & PERM_CAN_CREATE;
	}
	bool canSeeCategoryList() const
	{
		return this->permissions & PERM_CAN_SEE_CATEGORY_LIST;
	}
	bool canSeeLinksHere() const
	{
		return this->permissions & PERM_CAN_SEE_LINKS_HERE;
	}
	bool canSearch() const
	{
		return this->permissions & PERM_CAN_SEARCH;
	}
	bool canDelete() const
	{
		return this->permissions & PERM_CAN_DELETE;
	}
	bool canSeePageList() const
	{
		return this->permissions & PERM_CAN_SEE_PAGE_LIST;
	}

	bool canSetPagePerms() const
	{
		return this->permissions & PERM_CAN_SET_PAGE_PERMS;
	}

	bool isAdmin() const
	{
		return this->permissions == PERM_IS_ADMIN;
	}

	std::string toString() const
	{
		return Permissions::toString(this->permissions);
	}

	static std::string toString(int perms);

	bool operator==(const Permissions &o) const
	{
		return this->permissions == o.permissions;
	}
};

#endif // PERMISSIONS_H
