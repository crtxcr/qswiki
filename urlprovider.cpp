/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <string_view>
#include "urlprovider.h"

std::string replaceSingleVar(std::string where, std::string varname, std::string replacement)
{
	// TODO: Varreplacer is a bit of an overkill, isn't it?

	Varreplacer replacer("{");
	replacer.addKeyValue(varname, replacement);
	return replacer.parse(where);
}
std::string UrlProvider::replaceOnlyPage(std::string templatepart, std::string page)
{
	return replaceSingleVar(templatepart, "page", page);
}

std::string UrlProvider::index()
{
	return config->linkindex;
}

std::string UrlProvider::recentSorted(unsigned int limit, unsigned int offset, unsigned int sort)
{
	Varreplacer replace("{");
	replace.addKeyValue("limit", std::to_string(limit));
	replace.addKeyValue("offset", std::to_string(offset));
	replace.addKeyValue("sort", std::to_string(sort));
	return replace.parse(config->linkrecentsort);
}

std::string UrlProvider::allPages()
{
	return config->linkallpages;
}

std::string UrlProvider::allPages(std::string rendertype)
{
	return replaceSingleVar(config->linkallpagesrendertype, "type", rendertype);
}

std::string UrlProvider::allCats()
{
	return config->linkallcats;
}

std::string UrlProvider::page(std::string pagename)
{
	return replaceOnlyPage(config->linkpage, pagename);
}

std::string UrlProvider::pageByTitle(std::string title)
{
	return replaceSingleVar(config->linkpagebytitle, "title", utils::strreplace(title, " ", "-"));
}

std::string UrlProvider::linksHere(std::string pagename)
{
	return replaceOnlyPage(config->linkshere, pagename);
}

std::string UrlProvider::pageHistory(std::string pagename)
{
	return replaceOnlyPage(config->linkhistory, pagename);
}

std::string UrlProvider::pageHistorySort(std::string pagename, unsigned int limit, unsigned int offset,
										 unsigned int sort)
{
	Varreplacer replace("{");
	replace.addKeyValue("page", pagename);
	replace.addKeyValue("limit", std::to_string(limit));
	replace.addKeyValue("offset", std::to_string(offset));
	replace.addKeyValue("sort", std::to_string(sort));
	return replace.parse(config->linkhistorysort);
}

std::string UrlProvider::pageRevision(std::string pagename, unsigned int revision)
{
	Varreplacer replace("{");
	replace.addKeyValue("page", pagename);
	replace.addKeyValue("revisionid", std::to_string(revision));
	return replace.parse(config->linkrevision);
}

std::string UrlProvider::editPage(std::string pagename)
{
	return replaceOnlyPage(config->linkedit, pagename);
}

std::string UrlProvider::pageSettings(std::string pagename)
{
	return replaceOnlyPage(config->linksettings, pagename);
}

std::string UrlProvider::pageDelete(std::string pagename)
{
	return replaceOnlyPage(config->linkdelete, pagename);
}

std::string UrlProvider::userSettings()
{
	return config->usersettingsurl;
}

std::string UrlProvider::category(std::string catname)
{
	return replaceSingleVar(config->linkcategory, "category", catname);
}

std::string UrlProvider::category(std::string catname, std::string rendertype)
{
	Varreplacer replace("{");
	replace.addKeyValue("category", catname);
	replace.addKeyValue("type", rendertype);
	return replace.parse(config->linkcategoryrendertype);
}


std::string UrlProvider::login(std::string page)
{
	return replaceOnlyPage(config->loginurl, page);
}

std::string UrlProvider::rootUrl()
{
	return config->rooturl;
}

std::string UrlProvider::atomFeed(std::string filter)
{
	return combine({config->rooturl, replaceSingleVar(config->atomurl, "filter", filter)});
}

std::string UrlProvider::combine(std::initializer_list<std::string> urls)
{
	std::string result;
	for(const std::string &url : urls)
	{
		std::string_view urlview{url};
		if(result.back() == '/' && urlview.front() == '/')
		{
			urlview.remove_prefix(1);
		}
		result += urlview;
	}
	return result;
}
