/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <exception>
#include <iostream>
#include "config.h"
#include "permissions.h"
#include "varreplacer.h"

std::string Config::required(const std::string &key)
{
	auto it = this->configmap.find(key);
	if(it != this->configmap.end())
	{
		return it->second;
	}
	throw std::runtime_error("Required config key " + key + " not found");
}

std::string Config::optional(const std::string &key, std::string defaultvalue)
{
	auto it = this->configmap.find(key);
	if(it != this->configmap.end())
	{
		return it->second;
	}
	return defaultvalue;
}

int Config::optional(const std::string &key, int defaultvalue)
{
	auto it = this->configmap.find(key);
	if(it != this->configmap.end())
	{
		std::string str = it->second;
		return std::stoi(str);
	}
	return defaultvalue;
}

uint64_t Config::optional(const std::string &key, uint64_t defaultvalue)
{
	auto it = this->configmap.find(key);
	if(it != this->configmap.end())
	{
		std::string str = it->second;
		return static_cast<uint64_t>(std::stoull(str));
	}
	return defaultvalue;
}

Config::Config(const std::map<std::string, std::string> &map)
{

	this->configmap = map;
	this->wikipath = optional("wikipath", "/");
	this->handlersConfig.anon_username = optional("anon_username", "anonymouse");
	this->handlersConfig.wikiname = required("wikiname");
	this->logfile = required("logfile");
	this->templatepath = required("templatepath");
	this->urls.linkallcats = required("linkallcats");
	this->urls.linkallpages = required("linkallpages");
	this->urls.linkallpagesrendertype = required ("linkallpagesrendertype");
	this->urls.linkcategory = required("linkcategory");
	this->urls.linkcategoryrendertype = required("linkcategoryrendertype");
	this->urls.linkdelete = required("linkdelete");
	this->urls.linkedit = required("linkedit");
	this->urls.linkhistory = required("linkhistory");
	this->urls.linkindex = required("linkindex");
	this->urls.linklogout = required("linklogout");
	this->urls.linkpage = required("linkpage");
	this->urls.linkpagebytitle = required("linkpagebytitle");
	this->urls.linkrecent = required("linkrecent");
	this->urls.linkrevision = required("linkrevision");
	this->urls.linksettings = required("linksettings");
	this->urls.linkshere = required("linkshere");
	this->urls.loginurl = required("loginurl");
	this->urls.linkrecentsort = required("linkrecentsort");
	this->urls.linkhistorysort = required("linkhistorysort");
	this->urls.actionurl = required("actionurl");
	this->urls.settingsurl = required("settingsurl");
	this->urls.deletionurl = required("deletionurl");
	this->urls.adminregisterurl = required("adminregisterurl");
	this->urls.usersettingsurl = required("usersettingsurl");
	this->urls.rooturl = required("rooturl");
	this->urls.atomurl = required("atomurl");
	this->connectionstring = required("connectionstring");

	this->handlersConfig.max_pagename_length = optional("max_pagename_length", 256);
	this->session_max_lifetime = optional("session_max_lifetime", 3600);
	this->handlersConfig.query_limit = optional("query_limit", 200);
	this->handlersConfig.page_title_template = optional("page_title_template", "{title} - {wikiname}");
	this->threadscount = optional("threadscount", 1);

	this->handlersConfig.anon_permissions = Permissions(required("anon_permissions"));

	this->templateprefix = "{qswiki:";

	this->max_payload_length = optional("max_payload_length", 60 * 1024 * 1024);

	ConfigVariableResolver resolver{this->configmap};
	this->configVarResolver = resolver;

	Varreplacer replacer("{");
	replacer.addKeyValue("wikiname", this->handlersConfig.wikiname);
	this->handlersConfig.page_title_template = replacer.parse(this->handlersConfig.page_title_template);
}

ConfigReader::ConfigReader(const std::string &file)
{
	this->path = file;
}

Config ConfigReader::readConfig()
{
	std::fstream f1(path, std::fstream::in);
	std::string line;
	std::map<std::string, std::string> configmap;
	while(getline(f1, line))
	{
		if(isspace(line[0]) || line[0] == '#')
		{
			continue;
		}
		std::stringstream s(line);
		std::string key;
		std::string value;
		s >> key >> value;

		configmap.insert(std::make_pair(std::move(key), std::move(value)));
	}
	return Config(configmap);
}
