FROM alpine:edge
RUN apk update
RUN apk upgrade
RUN apk add sqlite-dev
RUN apk add openssl-dev
RUN apk add make g++ 
COPY . /app
WORKDIR /app
RUN adduser -D qswiki
RUN chown -R qswiki . 
RUN mkdir /config/ /cache/
USER qswiki
RUN make release
ENTRYPOINT /app/qswiki /config/qswiki.conf 
