#ifndef VARPARSER_H
#define VARPARSER_H
#include <string>
#include <string_view>
#include <functional>
#include <map>
#include <tuple>
class Varreplacer
{
  private:
	std::string_view content;
	std::string_view prefix;
	std::map<std::string_view, std::function<std::string(std::string_view)>> resolverFunctionsMap;
	std::map<std::string_view, std::string> keyValues;
	std::tuple<std::string_view, std::string_view> extractKeyAndValue(std::string_view var);
	std::string makeReplacement(std::string_view varkeyvalue);

  public:
	Varreplacer(std::string_view prefix);

	void addKeyValue(std::string_view key, std::string value);
	void addResolver(std::string_view key, std::function<std::string(std::string_view)> resolver);
	std::string parse(std::string_view content);
};

#endif // VARPARSER_H
