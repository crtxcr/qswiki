#ifndef LINKCREATOR_H
#define LINKCREATOR_H
#include "config.h"
#include "varreplacer.h"
class UrlProvider
{
  private:
	const ConfigUrls *config;

	std::string replaceOnlyPage(std::string templatepart, std::string page);

  public:
	UrlProvider(const ConfigUrls &config)
	{
		this->config = &config;
	}

	std::string index();

	std::string recent();

	std::string recentSorted(unsigned int limit, unsigned int offset, unsigned int sort);

	std::string allPages();
	std::string allPages(std::string rendertype);
	
	std::string allCats();

	std::string page(std::string pagename);

	std::string pageByTitle(std::string title);

	std::string linksHere(std::string pagename);

	std::string pageHistory(std::string pagename);

	std::string pageHistorySort(std::string pagename, unsigned int limit, unsigned int offset, unsigned int sort);

	std::string pageRevision(std::string pagename, unsigned int revision);

	std::string editPage(std::string pagename);

	std::string pageSettings(std::string pagename);

	std::string pageDelete(std::string pagename);

	std::string userSettings();

	std::string refreshSession();

	std::string category(std::string catname);
	std::string category(std::string catname, std::string rendertype);
	
	std::string login(std::string page);

	std::string rootUrl();

	std::string atomFeed(std::string filter);

	std::string combine(std::initializer_list<std::string> urls);
};

#endif // LINKCREATOR_H
