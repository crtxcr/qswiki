#ifndef PAGE_H
#define PAGE_H
#include <string>

class Page
{
  public:
	Page();
	std::string name;
	std::string title;
	std::string parentpage;
	bool listed;
	bool feedlisted;
	unsigned int current_revision;
	unsigned int pageid;
};

#endif // PAGE_H
