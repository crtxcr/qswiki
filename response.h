#ifndef RESPONSE_H
#define RESPONSE_H

#include <string>
#include <map>
#include <vector>
#include "cookie.h"
class Response
{
  private:
	int status_code = 200;
	std::string html;
	std::string content_type = "text/html";
	std::map<std::string, std::string> responseHeaders;
	std::vector<Cookie> cookies;

  public:
	Response();
	Response(int http_status_code, std::string html);

	int getStatus() const
	{
		return this->status_code;
	}
	std::string getBody() const
	{
		return this->html;
	}

	void addHeader(std::string key, std::string value);
	static Response redirectTemporarily(std::string url);

	void setStatus(int status)
	{
		this->status_code = status;
	}
	void setBody(std::string body)
	{
		this->html = body;
	}

	const std::map<std::string, std::string> &getResponseHeaders() const
	{
		return this->responseHeaders;
	}

	// TODO: maybe "getEffectiveResponseHeaders?" that would include cookies etc.

	const std::vector<Cookie> &getCookies() const
	{
		return this->cookies;
	}

	void addCookie(Cookie cookie)
	{
		this->cookies.push_back(cookie);
	}

	void setContentType(const std::string &type)
	{
		this->content_type = type;
	}

	std::string getContentType() const
	{
		return this->content_type;
	}
};

#endif // RESPONSE_H
