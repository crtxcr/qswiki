#ifndef IPARSER_H
#define IPARSER_H
#include <vector>
#include <string_view>
#include <functional>
#include "headline.h"
#include "database/pagedao.h"
#include "urlprovider.h"
class IParser
{
  protected:
	static std::string empty(std::string_view key, std::string_view content)
	{
		return "";
	}

  public:
	virtual std::string extractFirstTag(std::string tagname, const std::string &content) const = 0;
	virtual std::string extractCommand(std::string cmdname, const std::string &content) const = 0;
	virtual std::vector<std::string> extractCommands(std::string cmdname, const std::string &content) const = 0;

	virtual std::vector<Headline> extractHeadlines(const std::string &content) const = 0;
	virtual inline std::string parse(const PageDao &pagedao, UrlProvider &provider, const std::string &content) const
	{
		return parse(pagedao, provider, content, empty);
	}
	virtual std::string parse(const PageDao &pagedao, UrlProvider &provider, const std::string &content,
							  const std::function<std::string(std::string_view, std::string_view)> &callback) const = 0;

	virtual std::string parseDynamics(
		const std::string &content,
		const std::function<std::string(std::string_view, std::string_view)> &callback) const = 0;
	virtual std::vector<std::string> extractCategories(const std::string &content) const = 0;

	virtual ~IParser(){};
};

#endif // PARSER_H
