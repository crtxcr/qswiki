#ifndef CLISERVER_H
#define CLISERVER_H
#include <thread>
#include "cli.h"

class CLIServer
{
  private:
	CLIHandler *handler = nullptr;

  public:
	CLIServer(CLIHandler &handler);
	bool detachServer(std::string socketpath);
};

#endif // CLISERVER_H
