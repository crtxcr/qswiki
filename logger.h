#ifndef LOGGER_H
#define LOGGER_H
#include <iostream>
#include <chrono>
class Logger
{
  private:
	class LogEntry
	{
		bool headerSent = false;
		std::ostream *out = nullptr;
		std::string prefix;

	  public:
		LogEntry(std::ostream *out, std::string prefix) : out(out), prefix(prefix)
		{
		}

		template <class T> LogEntry &operator<<(const T &val)
		{
			if(out == nullptr)
				return *this;
			if(!headerSent)
			{
				(*out) << time(0) << " " << prefix;
			}
			(*out) << val;
			headerSent = true;
			return *this; // or maybe out itself? probably not.
		}
		~LogEntry()
		{
			if(out != nullptr)
			{
				(*out) << std::endl;
				(*out).flush();
			}
		}
	};

  public:
	static std::ostream *out;
	static int logLevel;
	static void setStream(std::ostream *out)
	{
		Logger::out = out;
	}

	static LogEntry debug()
	{
		if(Logger::logLevel >= 3)
			return LogEntry(out, "Debug: ");

		return LogEntry(nullptr, "");
	}

	static LogEntry error()
	{
		return LogEntry(out, "Error: ");
	}

	static LogEntry log()
	{
		if(Logger::logLevel >= 2)
			return LogEntry(out, "Log: ");

		return LogEntry(nullptr, "");
	}
};

#endif // LOGGER_H
