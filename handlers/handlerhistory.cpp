/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "handlerhistory.h"
#include "handler.h"
#include "../htmllink.h"
#include "../logger.h"
#include "../database/exceptions.h"
Response HandlerHistory::handleRequest(const Request &r)
{
	QueryOption qo = queryOption(r, DESCENDING);
	std::string page = r.get("page");
	std::string title;
	if(page.empty())
	{
		if(!this->userSession->user.permissions.canSeeGlobalHistory())
		{
			return errorResponse("Permission denied", "You can't see the changes history on this wiki", 403);
		}
		title = "History";
	}
	else
	{
		Permissions perms = effectivePermissions(page);
		if(!perms.canSeePageHistory())
		{
			return errorResponse("Permission denied", "You cannot see the changes history of this page", 403);
		}
		title = "History: " + page;
	}

	unsigned int count = 0;
	std::vector<Revision> resultList;
	auto revisionDao = this->database->createRevisionDao();

	auto makeSortedLink = [&](unsigned int limit, unsigned int offset, unsigned int order)
	{
		if(!page.empty())
		{
			return this->urlProvider->pageHistorySort(page, limit, offset, order);
		}
		return this->urlProvider->recentSorted(limit, offset, order);
	};
	std::string templatename = "recentchanges";
	try
	{
		if(!page.empty())
		{
			auto pageDao = this->database->createPageDao();
			if(!pageDao->exists(page))
			{
				return errorResponse("No such page", "No such page exists to show history for", 404);
			}
			count = revisionDao->countTotalRevisions(page);
			resultList = revisionDao->getAllRevisionsForPage(page, qo);
			templatename = "page_history";
		}
		else
		{
			count = revisionDao->countTotalRevisions();
			if(count == 0)
			{
				return errorResponse("No revisions", "This wiki does not have any pages with revisions yet");
			}
			resultList = revisionDao->getAllRevisions(qo);
		}
	}
	catch(const DatabaseException &e)
	{
		Logger::error() << "DatabaseException in handlerhistory: " << e.what();
		return errorResponse("Database error", "While trying to fetch revision list, a database error occured");
	}
	TemplatePage historyPage = this->templ->getPage(templatename);
	setGeneralVars(historyPage);

	if((qo.offset + (unsigned int)resultList.size()) < count)
	{
		HtmlLink link;
		link.href = makeSortedLink(qo.limit, qo.offset + qo.limit, qo.order);
		link.innervalue = "Next page";

		historyPage.setVar("nextpage", link.render());
	}

	unsigned int prevoffset = qo.offset - qo.limit;
	if(prevoffset > count)
	{
		prevoffset = 0;
	}
	if(qo.offset > 0 && qo.offset < count)
	{
		HtmlLink link;
		link.href = makeSortedLink(qo.limit, prevoffset, qo.order);
		link.innervalue = "Previous page";

		historyPage.setVar("prevpage", link.render());
	}

	unsigned int neworder = (qo.order == DESCENDING) ? ASCENDING : DESCENDING;
	historyPage.setVar("linkrecentsort", makeSortedLink(qo.limit, qo.offset, neworder));
	historyPage.setVar("revisionlist", this->templ->renderRevisionList(resultList, page.empty()));
	historyPage.setVar("title", createPageTitle(title));
	Response response;
	response.setBody(historyPage.render());
	response.setStatus(200);
	return response;
}

bool HandlerHistory::canAccess([[maybe_unused]] const Permissions &perms)
{
	return true; // This is a lie but we need to this a little more fine grained here, which we do in the handleRequest
}
