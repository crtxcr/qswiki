/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "handlerallpages.h"
#include "../grouper.h"
#include "../pagelistrenderer.h"

Response HandlerAllPages::handleRequest(const Request &r)
{
	try
	{
		Response response;
		auto pageDao = this->database->createPageDao();
		QueryOption qo = queryOption(r);
		std::vector<Page> pageList = pageDao->getPageList(qo);
		if(pageList.size() == 0)
		{
			return errorResponse("No pages", "This wiki does not have any pages yet");
		}
		
		PageListRenderer pagelistrender(*this->templ, *this->urlProvider, *this->database);
		TemplatePage allPages = this->templ->getPage("allpages");
		allPages.setVar("pagelistcontent", pagelistrender.render(pageList, r.get("rendertype")));
		allPages.setVar("pagelistletterlink", this->urlProvider->allPages(PageListRenderer::RENDER_GROUP_BY_LETTER));
		allPages.setVar("pagelistcreationdatelink",  this->urlProvider->allPages(PageListRenderer::RENDER_GROUP_BY_CREATIONDATE));
				
		allPages.setVar("title", createPageTitle("All pages"));
		setGeneralVars(allPages);
		response.setBody(allPages.render());
		response.setStatus(200);
		return response;
	}
	catch(std::exception &e)
	{
		Logger::error() << "Error during allpages Handler" << e.what();
		return errorResponse("Error", "An unknown error occured");
	}
}

std::string HandlerAllPages::accessErrorMessage()
{
	return "You don't permissions to list all pages";
}

bool HandlerAllPages::canAccess(const Permissions &perms)
{
	return perms.canSeePageList();
}
