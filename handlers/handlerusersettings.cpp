#include "handlerusersettings.h"
#include "../authenticator.h"
#include "../random.h"
#include "../database/exceptions.h"

Response HandlerUserSettings::handleRequest(const Request &r)
{
	if(r.getRequestMethod() == "POST")
	{
		if(r.post("do") == "submit")
		{
			std::string oldpassword = r.post("oldpassword");
			std::string newpassword = r.post("newpassword");
			std::string newpasswordconfirm = r.post("newpasswordconfirm");

			if(newpassword != newpasswordconfirm)
			{
				// TODO: is not nice, users has to hit the back button...
				return this->errorResponse("Passwords don't match", "The entered new passwords don't match");
			}
			auto userDao = this->database->createUserDao();
			Authenticator authenticator(*userDao);

			std::variant<User, AuthenticationError> authresult =
				authenticator.authenticate(this->userSession->user.login, oldpassword);
			if(std::holds_alternative<AuthenticationError>(authresult))
			{
				return this->errorResponse("Invalid current password", "The old password you entered is invalid");
			}
			Random r;
			std::vector<char> salt = r.getRandom(AUTH_DEFAULT_SALT_SIZE);
			User user = std::get<User>(authresult);
			user.salt = salt;
			user.password = authenticator.hash(newpassword, user.salt);
			if(user.password.empty())
			{
				Logger::error() << "Authenticator returned empty hash";
				return this->errorResponse("Error", "An error occured while trying to store new password");
			}
			try
			{
				userDao->save(user);
			}
			catch(const DatabaseException &e)
			{
				Logger::debug() << "Error saving user: " << e.what();
				return errorResponse("Error", "A database error occured while trying to save user with new settings");
			}

			return Response::redirectTemporarily(this->urlProvider->userSettings());
		}
	}

	TemplatePage userSettingsPage = this->templ->getPage("usersettings");
	setGeneralVars(userSettingsPage);
	userSettingsPage.setVar("usersettingsurl", urlProvider->userSettings());
	userSettingsPage.setVar("title", createPageTitle("User settings - " + this->userSession->user.login));
	Response result;
	result.setStatus(200);
	result.setBody(userSettingsPage.render());

	return result;
}

bool HandlerUserSettings::canAccess([[maybe_unused]] const Permissions &perms)
{
	return this->userSession->loggedIn;
}

std::string HandlerUserSettings::accessErrorMessage()
{
	return "Only logged-in users can change their settings";
}
