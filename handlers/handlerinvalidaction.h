#ifndef HANDLERINVALIDACTION_H
#define HANDLERINVALIDACTION_H
#include "handler.h"

class HandlerInvalidAction : public Handler
{
  public:
	Response handleRequest(const Request &r) override;
	~HandlerInvalidAction() override
	{
	}
	bool canAccess(const Permissions &perms) override;
	using Handler::Handler;
};

#endif // HANDLERINVALIDACTION_H
