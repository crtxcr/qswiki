/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "handlersearch.h"
Response HandlerSearch::handleRequest(const Request &r)
{
	Response response;
	std::string q = r.get("q");
	if(q.empty())
	{
		TemplatePage searchForm = this->templ->getPage("searchform");
		response.setBody(searchForm.render());
		response.setStatus(200);
		setGeneralVars(searchForm);
		return response;
	}

	auto pageDao = this->database->createPageDao();
	QueryOption qo = queryOption(r);
	try
	{
		auto resultList = pageDao->search(q, qo);
		if(resultList.size() == 0)
		{
			return errorResponse("No results", "Your search for " + q + " did not yield any results.");
		}
		TemplatePage searchPage = this->templ->getPage("search");
		std::string body = this->templ->renderSearch(resultList);
		searchPage.setVar("pagelist", body);
		searchPage.setVar("searchterm", q);
		setGeneralVars(searchPage);
		searchPage.setVar("title", createPageTitle("Search"));
		response.setBody(searchPage.render());
		response.setStatus(200);
		return response;
	}
	catch(std::exception &e)
	{
		Logger::error() << "Search failed, q: " << q << "Error: " << e.what();
		return errorResponse("Technical Error", "The system failed to perform your search");
	}
}

bool HandlerSearch::canAccess(const Permissions &perms)
{
	return perms.canSearch();
}

std::string HandlerSearch::accessErrorMessage()
{
	return "You are not allowed to search this wiki";
}
