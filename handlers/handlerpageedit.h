#ifndef HANDLERPAGEEDI_H
#define HANDLERPAGEEDI_H

#include "handlerpage.h"
#include "../page.h"

class HandlerPageEdit : public HandlerPage
{
  protected:
	bool pageMustExist() override;
	bool canAccess(std::string page) override;

  public:
	Response handleRequest(PageDao &pageDao, std::string pagename, const Request &r) override;

	~HandlerPageEdit() override
	{
	}
	using HandlerPage::HandlerPage;
};

#endif // HANDLERPAGEEDI_H
