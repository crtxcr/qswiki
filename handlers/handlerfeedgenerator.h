#ifndef HANDLERFEEDGENERATOR_H
#define HANDLERFEEDGENERATOR_H
#include "handler.h"
#include "../page.h"
#include "../revision.h"
class HandlerFeedGenerator : public Handler
{
	typedef std::pair<Page, Revision> EntryRevisionPair;

  protected:
	std::vector<EntryRevisionPair> fetchEntries(std::vector<std::string> categories);
	std::string generateAtom(const std::vector<EntryRevisionPair> &entries, std::string atomtitle);
	Response generateRss(const std::vector<EntryRevisionPair> &entries);

  public:
	using Handler::Handler;
	Response handleRequest(const Request &r) override;
	bool canAccess(const Permissions &perms) override;
};

#endif // HANDLERFEEDGENERATOR_H
