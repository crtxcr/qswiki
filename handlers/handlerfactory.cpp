/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "handlerfactory.h"
#include "handler.h"
#include "handlerdefault.h"
#include "handlerpageview.h"
#include "handlerinvalidaction.h"
#include "handlerlogin.h"
#include "handlerpageedit.h"
#include "handlersearch.h"
#include "handlerallpages.h"
#include "handlerallcategories.h"
#include "handlercategory.h"
#include "handlerhistory.h"
#include "handlerpagedelete.h"
#include "handlerusersettings.h"
#include "handlerversion.h"
#include "handlerfeedgenerator.h"
std::unique_ptr<Handler> HandlerFactory::createHandler(const std::string &action, Session &userSession)
{
	if(action == "" || action == "index")
	{
		return produce<HandlerDefault>(userSession);
	}
	if(action == "show")
	{
		return produce<HandlerPageView>(userSession);
	}
	if(action == "edit")
	{
		return produce<HandlerPageEdit>(userSession);
	}
	if(action == "login")
	{
		return produce<HandlerLogin>(userSession);
	}
	if(action == "search")
	{
		return produce<HandlerSearch>(userSession);
	}
	if(action == "delete")
	{
		return produce<HandlerPageDelete>(userSession);
	}
	if(action == "allpages")
	{
		return produce<HandlerAllPages>(userSession);
	}
	if(action == "allcategories")
	{
		return produce<HandlerAllCategories>(userSession);
	}
	if(action == "showcat")
	{
		return produce<HandlerCategory>(userSession);
	}
	if(action == "recent")
	{
		return produce<HandlerHistory>(userSession);
	}
	if(action == "usersettings")
	{
		return produce<HandlerUserSettings>(userSession);
	}
	if(action == "version")
	{
		return produce<HandlerVersion>(userSession);
	}
	if(action == "feed")
	{
		return produce<HandlerFeedGenerator>(userSession);
	}

	return produce<HandlerInvalidAction>(userSession);
}
