#ifndef HANDLERDEFAULT_H
#define HANDLERDEFAULT_H

#include "handler.h"
class HandlerDefault : public Handler
{
  public:
	Response handleRequest(const Request &r) override;
	~HandlerDefault() override;
	using Handler::Handler;
	bool canAccess(const Permissions &perms) override;
};

#endif // HANDLERDEFAULT_H
