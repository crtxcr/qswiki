#ifndef HANDLERALLCATEGORIES_H
#define HANDLERALLCATEGORIES_H

#include "handler.h"

class HandlerAllCategories : public Handler
{
  public:
	HandlerAllCategories();
	using Handler::Handler;
	Response handleRequest(const Request &r) override;
	std::string accessErrorMessage() override;
	bool canAccess(const Permissions &perms) override;
};

#endif // HANDLERALLCATEGORIES_H
