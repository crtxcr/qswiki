#ifndef HANDLERPAGE_H
#define HANDLERPAGE_H
#include "handler.h"

class HandlerPage : public Handler
{
  protected:
	virtual bool canAccess(std::string page) = 0;
	virtual bool pageMustExist();
	virtual std::string accessErrorMessage();

  public:
	Response handle(const Request &r) override;
	virtual Response handleRequest(PageDao &pageDao, std::string pagename, const Request &r) = 0;
	~HandlerPage() override
	{
	}
	using Handler::Handler;

	void setPageVars(TemplatePage &page, std::string pagename);
};

#endif // HANDLERPAGE_H
