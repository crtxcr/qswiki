#ifndef HANDLERCATEGORY_H
#define HANDLERCATEGORY_H
#include "handler.h"

class HandlerCategory : public Handler
{
  public:
	HandlerCategory();
	using Handler::Handler;
	Response handleRequest(const Request &r) override;
	std::string accessErrorMessage() override;
	bool canAccess(const Permissions &perms) override;
};

#endif // HANDLERCATEGORY_H
