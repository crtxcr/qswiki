#ifndef HANDLERSEARCH_H
#define HANDLERSEARCH_H
#include <vector>
#include "handler.h"
class HandlerSearch : public Handler
{
  public:
	HandlerSearch();
	using Handler::Handler;
	Response handleRequest(const Request &r) override;
	bool canAccess(const Permissions &perms) override;
	std::string accessErrorMessage();
};

#endif // HANDLERSEARCH_H
