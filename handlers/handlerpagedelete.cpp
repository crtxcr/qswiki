/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "handlerpagedelete.h"
#include "../database/exceptions.h"

bool HandlerPageDelete::pageMustExist()
{
	return true;
}

bool HandlerPageDelete::canAccess(std::string page)
{
	return effectivePermissions(page).canDelete();
}

std::string HandlerPageDelete::accessErrorMessage()
{
	return "You don't have permission to delete pages";
}

Response HandlerPageDelete::handleRequest(PageDao &pageDao, std::string pagename, const Request &r)
{
	try
	{

		if(r.getRequestMethod() == "POST")
		{
			pageDao.deletePage(pagename);
			this->cache->removePrefix("page:"); // TODO: overkill?
			this->cache->removePrefix("feed:");
			return Response::redirectTemporarily(this->urlProvider->index());
		}
		TemplatePage delPage = this->templ->getPage("page_deletion");
		delPage.setVar("deletionurl", this->urlProvider->pageDelete(pagename));
		setPageVars(delPage, pagename);
		delPage.setVar("title", createPageTitle("Delete:" + pagename));
		Response r;
		r.setBody(delPage.render());
		return r;
	}
	catch(const DatabaseException &e)
	{
		Logger::debug() << "Error delete page: " << e.what();
		return errorResponse("Database error", "A database error occured while trying to delete this page");
	}
}
