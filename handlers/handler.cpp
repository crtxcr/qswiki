/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "handler.h"

void Handler::setGeneralVars(TemplatePage &page)
{
	if(userSession->loggedIn)
	{
		page.setVar("loginstatus", "Logged in as " + userSession->user.login);
	}
	else
	{
		page.setVar("loginstatus", "not logged in");
	}
	page.setVar("csrf_token", utils::toString(this->userSession->csrf_token));
}
Response Handler::errorResponse(std::string errortitle, std::string errormessage, int status)
{
	TemplatePage error = this->templ->getPage("error");
	error.setVar("title", createPageTitle(errortitle));
	error.setVar("errortitle", errortitle);
	error.setVar("errormessage", errormessage);
	// TODO: log?
	setGeneralVars(error);
	return {status, error.render()};
}

std::string Handler::createPageTitle(std::string title)
{
	Varreplacer replacer("{");
	replacer.addKeyValue("title", title);
	return replacer.parse(this->handlersConfig->page_title_template);
}

QueryOption Handler::queryOption(const Request &r, SORT_ORDER defaultSort) const
{
	QueryOption result;
	result.includeUnlisted = false;
	try
	{
		result.limit = utils::toUInt(r.get("limit"));
	}
	catch(std::exception &e)
	{
		result.limit = 0;
	}
	try
	{
		result.offset = utils::toUInt(r.get("offset"));
	}
	catch(std::exception &e)
	{
		result.offset = 0;
	}
	std::string order = r.get("sort");
	result.order = defaultSort;
	if(order != "")
	{
		if(order == "1")
		{
			result.order = DESCENDING;
		}
		else
		{
			result.order = ASCENDING;
		}
	}

	return result;
}

Response Handler::handle(const Request &r)
{
	if(!canAccess(this->userSession->user.permissions))
	{
		return errorResponse("Permission denied", accessErrorMessage());
	}
	return handleRequest(r);
}

Permissions Handler::effectivePermissions(std::string page)
{
	Permissions &userPerms = this->userSession->user.permissions;
	if(userPerms.isAdmin())
	{
		return userPerms;
	}
	return this->database->createPermissionsDao()->find(page, this->userSession->user.login).value_or(userPerms);
}
