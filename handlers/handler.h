#ifndef HANDLER_H
#define HANDLER_H
#include <memory>
#include "../config.h"
#include "../response.h"
#include "../request.h"
#include "../template.h"
#include "../database/database.h"
#include "../urlprovider.h"
#include "../database/queryoption.h"
#include "../logger.h"
#include "../cache/icache.h"
#include "../dynamic/dynamiccontent.h"

class Handler
{
  protected:
	ICache *cache;
	Template *templ;
	Database *database;
	Session *userSession;
	UrlProvider *urlProvider;
	HandlerConfig *handlersConfig;

	// TODO: may not to find a better place for this method
	Permissions effectivePermissions(std::string page);
	QueryOption queryOption(const Request &r, SORT_ORDER defaultSort = ASCENDING) const;

  public:
	Handler(HandlerConfig &handlersConfig, Template &templ, Database &db, Session &userSession, UrlProvider &provider,
			ICache &cache)
	{
		this->handlersConfig = &handlersConfig;
		this->templ = &templ;
		this->database = &db;
		this->userSession = &userSession;
		this->urlProvider = &provider;
		this->cache = &cache;
	}

	virtual Response handle(const Request &r);
	virtual Response handleRequest([[maybe_unused]] const Request &r)
	{
		return this->errorResponse("Invalid action", "This action is not implemented yet");
	}

	virtual bool canAccess([[maybe_unused]] const Permissions &perms)
	{
		return false;
	}
	virtual std::string accessErrorMessage()
	{
		return "No permission for this action";
	}
	void setGeneralVars(TemplatePage &page);
	virtual ~Handler()
	{
	}

	template <class T> inline std::shared_ptr<T> createDynamic()
	{
		return std::make_shared<T>(*this->templ, *this->database, *this->urlProvider);
	}

	Response errorResponse(std::string errortitle, std::string errormessage, int status = 200);
	std::string createPageTitle(std::string append);
};

#endif // HANDLER_H
