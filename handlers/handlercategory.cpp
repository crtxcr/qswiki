/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "handlercategory.h"
#include "../pagelistrenderer.h"

Response HandlerCategory::handleRequest(const Request &r)
{
	try
	{
		Response response;
		std::string categoryname = r.get("category");
		auto categoryDao = this->database->createCategoryDao();
		if(!categoryDao->find(categoryname))
		{
			return this->errorResponse("No such category", "A category with the provided name does not exist", 404);
		}
		QueryOption qo = queryOption(r);
		auto resultList = categoryDao->fetchMembers(categoryname, qo);
		TemplatePage searchPage = this->templ->getPage("show_category");
		PageListRenderer pagelistrender(*this->templ, *this->urlProvider, *this->database);

		std::string body = pagelistrender.render(resultList, r.get("rendertype"));
		searchPage.setVar("pagelistcontent", body);
		searchPage.setVar("pagelistletterlink", this->urlProvider->category(categoryname, PageListRenderer::RENDER_GROUP_BY_LETTER));
		searchPage.setVar("pagelistcreationdatelink",  this->urlProvider->category(categoryname, PageListRenderer::RENDER_GROUP_BY_CREATIONDATE));
		searchPage.setVar("categoryname", categoryname);
		setGeneralVars(searchPage);
		searchPage.setVar("title", createPageTitle("Category: " + categoryname));
		response.setBody(searchPage.render());
		response.setStatus(200);
		return response;
	}
	catch(std::exception &e)
	{
		Logger::error() << "Error during category Handler" << e.what();
		return errorResponse("Error", "An unknown error occured");
	}
}

std::string HandlerCategory::accessErrorMessage()
{
	return "You don't have permission to view categories";
}

bool HandlerCategory::canAccess(const Permissions &perms)
{
	return perms.canRead(); // TODO: we may need a more specific permission
}
