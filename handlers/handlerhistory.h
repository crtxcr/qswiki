#ifndef HANDLERHISTORY_H
#define HANDLERHISTORY_H
#include "handler.h"

class HandlerHistory : public Handler
{

  public:
	HandlerHistory();
	using Handler::Handler;
	Response handleRequest(const Request &r) override;
	bool canAccess(const Permissions &perms) override;
};

#endif // HANDLERHISTORY_H
