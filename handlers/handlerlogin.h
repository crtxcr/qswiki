#ifndef HANDLERLOGIN_H
#define HANDLERLOGIN_H
#include <vector>
#include "handler.h"

class HandlerLogin : public Handler
{
  public:
	HandlerLogin();
	Response handleRequest(const Request &r) override;
	~HandlerLogin() override
	{
	}
	bool canAccess(const Permissions &perms) override;
	using Handler::Handler;
};

#endif // HANDERLOGIN_H
