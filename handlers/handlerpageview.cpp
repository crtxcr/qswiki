/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "handlerpageview.h"
#include "../database/exceptions.h"
#include "../logger.h"
#include "../parser.h"
#include "../htmllink.h"
#include "../dynamic/dynamiccontentpostlist.h"
#include "../dynamic/dynamiccontentincludepage.h"
#include "../dynamic/dynamiccontentsetvar.h"
#include "../dynamic/dynamiccontentgetvar.h"
#include "../revisionrenderer.h"

bool HandlerPageView::canAccess(std::string page)
{
	return effectivePermissions(page).canRead();
}

std::string HandlerPageView::accessErrorMessage()
{
	return "You don't have permission to view this page";
}

std::string HandlerPageView::createIndexContent(IParser &parser, std::string content)
{
	std::vector<Headline> headlines = parser.extractHeadlines(content);
	std::string indexcontent = "";
	unsigned int previous = 0;
	for(const Headline &h : headlines)
	{
		if(h.level > previous)
		{
			indexcontent += "<ul>";
		}
		else if(h.level < previous)
		{
			unsigned int diff = previous - h.level;
			for(unsigned int i = 0; i < diff; i++)
			{
				indexcontent += "</ul>";
			}
		}
		previous = h.level;
		HtmlLink link;
		link.href = "#" + utils::strreplace(h.title, " ", "");
		link.innervalue = h.title;
		link.cssclass = "indexlink";
		indexcontent += "<li>" + link.render() + "</li>";
	}
	for(unsigned int i = 0; i < previous; i++)
	{
		indexcontent += "</ul>";
	}
	return indexcontent;
}

Response HandlerPageView::handleRequest(PageDao &pageDao, std::string pagename, const Request &r)
{

	std::string revisionparam = r.get("revision");
	unsigned int revisionid = 0;
	if(!revisionparam.empty())
	{
		try
		{
			revisionid = utils::toUInt(revisionparam);
		}
		catch(const std::exception &e)
		{
			return errorResponse("Error", "Supplied revisionid is misformated");
		}
	}

	std::optional<Revision> revision;
	std::string templatepartname;
	auto revisionDao = this->database->createRevisionDao();
	try
	{
		if(revisionid > 0)
		{
			if(!effectivePermissions(pagename).canSeePageHistory())
			{
				auto current = revisionDao->getCurrentForPage(pagename);
				if(current && current->revision > revisionid)
				{
					return errorResponse("Error", "You are not allowed to view older revisions of this page");
				}
			}
			revision = revisionDao->getRevisionForPage(pagename, revisionid);
			if(!revision)
			{
				return errorResponse("Revision not found", "No such revision found");
			}
			templatepartname = "page_view_revision";
		}
		else
		{
			if(!this->userSession->loggedIn)
			{
				auto content = this->cache->get("page:foranon:" + pagename);
				if(content)
				{
					Response r;
					r.setBody(*content);
					// TODO: etag?
					return r;
				}
			}
			revision = revisionDao->getCurrentForPage(pagename);
			templatepartname = "page_view";
		}
	}
	catch(const DatabaseException &e)
	{
		Logger::error() << "DatabaseException in handlerpageview: " << e.what();
		return errorResponse("Database error", "While trying to fetch revision, a database error occured");
	}

	Parser parser;
	Response result;
	result.setStatus(200);

	RevisionRenderer revisionRenderer{*this->templ, *this->database, *this->urlProvider, *this->userSession};

	std::string customtitle = parser.extractCommand("pagetitle", revision->content);
	std::string parsedcontent = revisionRenderer.renderContent(revision.value(), customtitle);
	/* TODO: Dynamic includes not considered, probably fine in practise */
	std::string indexcontent = createIndexContent(parser, revision->content);

	std::string revisionstr = std::to_string(revision->revision);
	TemplatePage page = this->templ->getPage(templatepartname);
	page.setVar("content", parsedcontent);
	page.setVar("index", indexcontent);
	page.setVar("editedby", revision->author);
	page.setVar("editedon", utils::toISODateTime(revision->timestamp));
	page.setVar("historyurl", this->urlProvider->pageHistory(pagename));
	page.setVar("revision", revisionstr);
	setPageVars(page, pagename);
	if(!customtitle.empty())
	{
		page.setVar("title", createPageTitle(customtitle));
	}
	std::string body = page.render();
	if(revisionid == 0 && !this->userSession->loggedIn)
	{
		this->cache->put("page:foranon:" + pagename, body);
	}
	result.addHeader("ETAG", revisionstr + "+" + std::to_string(this->userSession->loggedIn));
	result.setBody(body);
	return result;
}
