#ifndef HANDLERFACTORY_H
#define HANDLERFACTORY_H
#include <memory>
#include "handler.h"
#include "../template.h"
class HandlerFactory
{
	HandlerConfig &handlerConfig;
	Template &templ;
	Database &db;
	UrlProvider &urlProvider;
	ICache &cache;

	template <class T> inline std::unique_ptr<T> produce(Session &userSession)
	{
		return std::make_unique<T>(handlerConfig, templ, db, userSession, urlProvider, cache);
	}

  public:
	HandlerFactory(HandlerConfig &handlerConfig, Template &templ, Database &db, UrlProvider &urlprovider, ICache &cache)
		: handlerConfig(handlerConfig), templ(templ), db(db), urlProvider(urlprovider), cache(cache)
	{
	}
	std::unique_ptr<Handler> createHandler(const std::string &action, Session &userSession);
};

#endif // HANDLERFACTORY_H
