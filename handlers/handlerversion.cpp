#include "handlerversion.h"
#include "../version.h"
Response HandlerVersion::handleRequest([[maybe_unused]] const Request &r)
{
	Response response;
	response.setContentType("text/plain");
	response.setBody(get_version_string());
	return response;
}
