/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <optional>
#include "handlerpage.h"

Response HandlerPage::handle(const Request &r)
{
	std::string pagename = r.get("page");
	auto pageDao = this->database->createPageDao();
	if(pagename.empty())
	{
		std::string title = r.get("title");
		if(title.empty())
		{
			return errorResponse("No page given", "No page given to request");
		}
		title = utils::strreplace(title, "-", " ");
		auto page = pageDao->findByTitle(title);
		if(!page)
		{
			return errorResponse("No page by such title", "No page with such title exists");
		}
		pagename = page->name;
	}

	if(pageMustExist() && !pageDao->exists(pagename))
	{
		std::string createlink = this->urlProvider->editPage(pagename);
		return errorResponse(
			"Page not found",
			"The requested page was not found. Do you want to <a href=\"" + createlink + "\">create</a> it?", 404);
	}

	if(!canAccess(pagename))
	{
		return errorResponse("Permission denied", accessErrorMessage());
	}

	return this->handleRequest(*pageDao, pagename, r);
}

std::string HandlerPage::accessErrorMessage()
{
	return "You don't have permission to access this page";
}

bool HandlerPage::pageMustExist()
{
	return true;
}

void HandlerPage::setPageVars(TemplatePage &page, std::string pagename)
{
	setGeneralVars(page);

	if(!pagename.empty())
	{
		std::string headerlinks;
		TemplatePage headerlink = this->templ->getPage("_headerlink");
		auto addHeaderLink = [&headerlinks, &headerlink](std::string href, std::string value)
		{
			headerlink.setVar("href", href);
			headerlink.setVar("value", value);
			headerlinks += headerlink.render();
		};
		Permissions &perms = this->userSession->user.permissions;

		if(perms.canEdit())
		{
			addHeaderLink(this->urlProvider->editPage(pagename), "Edit");
			addHeaderLink(this->urlProvider->pageSettings(pagename), "Page settings");
		}
		if(perms.canDelete())
		{
			addHeaderLink(this->urlProvider->pageDelete(pagename), "Delete");
		}
		if(perms.canSeePageHistory())
		{
			addHeaderLink(this->urlProvider->pageHistory(pagename), "Show history");
		}

		page.setVar("headerlinks", headerlinks);
		page.setVar("page", pagename);
		page.setVar("title", createPageTitle(pagename));
	}
}
