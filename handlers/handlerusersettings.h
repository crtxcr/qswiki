#ifndef HANDLERUSERSETTINGS_H
#define HANDLERUSERSETTINGS_H
#include "handler.h"
class HandlerUserSettings : public Handler
{
  public:
	using Handler::Handler;
	Response handleRequest(const Request &r);
	bool canAccess(const Permissions &perms);
	std::string accessErrorMessage();
};

#endif // HANDLERUSERSETTINGS_H
