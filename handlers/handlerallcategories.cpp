/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "handlerallcategories.h"
#include "../urlprovider.h"
#include "../logger.h"
Response HandlerAllCategories::handleRequest(const Request &r)
{
	auto categoryDao = this->database->createCategoryDao();
	QueryOption qo = queryOption(r);
	auto resultList = categoryDao->fetchList(qo);
	if(resultList.size() == 0)
	{
		return errorResponse(
			"No categories",
			"This wiki does not have any categories defined yet or your query options did not yield any results");
	}
	TemplatePage searchPage = this->templ->getPage("allcategories");
	std::string body =
		this->templ->renderSearch(resultList, [&](std::string str) { return this->urlProvider->category(str); });
	searchPage.setVar("categorylist", body);
	searchPage.setVar("title", createPageTitle("All categories"));
	setGeneralVars(searchPage);

	Response response;
	response.setBody(searchPage.render());
	response.setStatus(200);
	return response;
}

std::string HandlerAllCategories::accessErrorMessage()
{
	return "You don't have permission to list all categories";
}

bool HandlerAllCategories::canAccess(const Permissions &perms)
{
	return perms.canSeeCategoryList();
}
