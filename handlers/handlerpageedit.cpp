/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "handlerpageedit.h"
#include "../database/exceptions.h"
#include "../request.h"

#include "../parser.h"
#include "../revisionrenderer.h"

bool HandlerPageEdit::canAccess([[maybe_unused]] std::string page)
{
	return effectivePermissions(page).canEdit();
}

bool HandlerPageEdit::pageMustExist()
{
	return false;
}

Response HandlerPageEdit::handleRequest(PageDao &pageDao, std::string pagename, const Request &r)
{
	bool pageexists = pageDao.exists(pagename);
	if(!pageexists && !this->userSession->user.permissions.canCreate())
	{
		return errorResponse("No permission", "You don't have permission to create new pages");
	}
	auto revisiondao = this->database->createRevisionDao();
	auto revision = revisiondao->getCurrentForPage(pagename);
	std::string body;

	unsigned int current_revision = 0;
	if(revision)
	{
		body = revision->content;
		current_revision = revision->revision;
	}
	std::string from = r.get("frompage");
	if(!from.empty())
	{
		if(!effectivePermissions(from).canRead())
		{
			return this->errorResponse("Permission denied",
									   "No access permissions, so you can't use this page as a template");
		}
		body = revisiondao->getCurrentForPage(from)->content;
	}
	if(r.getRequestMethod() == "POST")
	{
		if(r.post("do") == "submit")
		{
			std::string newContent = r.post("content");
			std::string newComment = r.post("comment");

			// TODO: must check, whether categories differ, and perhaps don't allow every user
			// to set categories
			Parser parser;
			std::vector<std::string> cats = parser.extractCategories(newContent);
			try
			{
				this->database->beginTransaction();

				std::string visiblecmd = parser.extractCommand("visible", newContent);
				std::string listedcmd = parser.extractCommand("listed", newContent);
				/* Backwarts compatibility */
				if(listedcmd.empty())
				{
					listedcmd = visiblecmd;
				}
				std::string feedlistedcmd = parser.extractCommand("feedlisted", newContent);

				std::string rename = parser.extractCommand("rename", newContent);
				std::string customtitle = parser.extractCommand("pagetitle", newContent);
				std::string parentpage = parser.extractCommand("parentpage", newContent);
				std::vector<std::string> perms = parser.extractCommands("permissions", newContent);

				if(parentpage != "" && !pageDao.find(parentpage))
				{
					return this->errorResponse("Invalid parent",
											   "Specified parent page " + parentpage + " does not exist");
				}

				Page page;
				std::optional<Page> currentPage = pageDao.find(pagename);
				if(currentPage)
				{
					page = *currentPage;
				}
				if(rename != "")
				{
					if(newComment == "")
					{
						newComment = "(renamed) from " + pagename + " to " + rename;
					}
					pagename = rename;
				}

				std::vector<std::pair<std::string, Permissions>> collectedPermissions;

				auto permissionDao = this->database->createPermissionsDao();
				for(const std::string &perm : perms)
				{
					auto splitted = utils::split(perm, '|');
					if(splitted.size() != 2)
					{
						return this->errorResponse("Invalid command", "permissions command is misformated");
					}
					auto currentPermission = permissionDao->find(pagename, splitted[0]);

					Permissions newPermissions = Permissions{splitted[1]};
					if(!currentPermission || newPermissions != currentPermission.value())
					{
						if(!this->userSession->user.permissions.canSetPagePerms())
						{
							this->database->rollbackTransaction();
							return errorResponse("Permission denied",
												 "You don't have permission to change permissions. Don't touch the "
												 "permission commands");
						}
					}
					collectedPermissions.emplace_back(splitted[0], newPermissions);
				}

				if(this->userSession->user.permissions.canSetPagePerms())
				{
					permissionDao->clearForPage(pagename);
					for(auto &perms : collectedPermissions)
					{
						permissionDao->save(pagename, perms.first, perms.second);
					}
				}

				page.current_revision = current_revision;
				page.listed = !(listedcmd == "0");
				page.feedlisted = !(feedlistedcmd == "0");
				page.name = pagename;
				page.title = customtitle;
				page.parentpage = parentpage;
				if(page.title.empty())
				{
					page.title = page.name;
				}
				pageDao.save(page);

				Revision newRevision;
				newRevision.author = this->userSession->user.login;
				newRevision.comment = newComment;
				newRevision.page = pagename;
				newRevision.content = newContent;

				revisiondao->save(newRevision);
				pageDao.setCategories(pagename, cats);
				this->database->commitTransaction();
				this->cache->removePrefix("page:"); // TODO: overkill?
				this->cache->removePrefix("feed:");
			}
			catch(const DatabaseException &e)
			{
				this->database->rollbackTransaction();
				Logger::debug() << "Error saving revision: " << e.what();
				return errorResponse("Database error", "A database error occured while trying to save this revision");
			}

			return Response::redirectTemporarily(urlProvider->page(pagename));
		}
		if(r.post("do") == "preview")
		{
			std::string newContent = r.post("content");
			Parser parser;
			std::string title = parser.extractCommand("pagetitle", newContent);
			TemplatePage templatePage = this->templ->getPage("page_creation_preview");
			templatePage.setVar("actionurl", urlProvider->editPage(pagename));

			RevisionRenderer revisionRenderer{*this->templ, *this->database, *this->urlProvider, *this->userSession};

			templatePage.setVar("preview_content", revisionRenderer.renderContent(newContent));
			templatePage.setVar("content", newContent);
			setPageVars(templatePage, pagename);
			templatePage.setVar("title", createPageTitle("Preview: " + title));
			templatePage.setVar("comment", r.post("comment"));
			Response response;
			response.setBody(templatePage.render());
			return response;
		}
	}

	TemplatePage templatePage = this->templ->getPage("page_creation");
	templatePage.setVar("actionurl", urlProvider->editPage(pagename));
	templatePage.setVar("content", body);
	setPageVars(templatePage, pagename);
	templatePage.setVar("title", createPageTitle("Edit: " + pagename));
	Response response;
	response.setBody(templatePage.render());
	return response;
}
