#ifndef HANDLERPAGEDELETE_H
#define HANDLERPAGEDELETE_H
#include "handlerpage.h"

class HandlerPageDelete : public HandlerPage
{
	bool pageMustExist() override;

	bool canAccess(std::string page) override;

	std::string accessErrorMessage() override;

  public:
	Response handleRequest(PageDao &pageDao, std::string pagename, const Request &r) override;
	using HandlerPage::HandlerPage;
};

#endif // HANDLERPAGEDELETE_H
