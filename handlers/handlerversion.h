#ifndef HANDLERVERSION_H
#define HANDLERVERSION_H
#include "handler.h"
class HandlerVersion : public Handler
{
  public:
	using Handler::Handler;

  public:
	Response handleRequest(const Request &r) override;

	bool canAccess([[maybe_unused]] const Permissions &perms) override
	{
		return true;
	}
};

#endif // HANDLERVERSION_H
