#ifndef HANDLERALLPAGES_H
#define HANDLERALLPAGES_H

#include "handler.h"
class HandlerAllPages : public Handler
{
  public:
	HandlerAllPages();
	using Handler::Handler;
	Response handleRequest(const Request &r) override;
	std::string accessErrorMessage() override;
	bool canAccess(const Permissions &perms) override;
};

#endif // HANDLERALLPAGES_H
