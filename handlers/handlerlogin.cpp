/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <atomic>
#include <mutex>
#include "handlerlogin.h"
#include "../logger.h"
#include "../authenticator.h"

Response HandlerLogin::handleRequest(const Request &r)
{
	if(r.param("submit") == "1")
	{
		std::string password = r.post("password");
		std::string username = r.post("user");

		auto userDao = this->database->createUserDao();
		Authenticator authenticator(*userDao);

		std::variant<User, AuthenticationError> authresult = authenticator.authenticate(username, password, r.getIp());
		if(std::holds_alternative<User>(authresult))
		{
			User user = std::get<User>(authresult);
			*(this->userSession) = Session(user);
			Response r = Response::redirectTemporarily(urlProvider->index());
			return r;
		}
		AuthenticationError error = std::get<AuthenticationError>(authresult);
		if(error == AuthenticationError::UserDisabled)
		{
			return errorResponse("Login failed", "The user account has been disabled");
		}

		return errorResponse("Login failed", "Login with supplied credentials failed");
	}
	std::string page = r.get("page");
	if(page.empty())
	{
		page = "index";
	}
	TemplatePage loginTemplatePage = this->templ->getPage("login");
	setGeneralVars(loginTemplatePage);
	loginTemplatePage.setVar("loginurl", urlProvider->login(page));
	loginTemplatePage.setVar("title", createPageTitle("Login"));
	Response result;
	result.setStatus(200);
	result.setBody(loginTemplatePage.render());

	return result;
}

bool HandlerLogin::canAccess([[maybe_unused]] const Permissions &perms)
{
	return true;
}
