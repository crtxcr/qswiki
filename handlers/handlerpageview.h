#ifndef HANDLERPAGEVIEW_H
#define HANDLERPAGEVIEW_H

#include "handler.h"
#include "handlerpage.h"
#include "../page.h"
#include "../iparser.h"
class HandlerPageView : public HandlerPage
{
  protected:
	bool canAccess(std::string page) override;
	std::string accessErrorMessage() override;
	std::string createIndexContent(IParser &parser, std::string content);

  public:
	Response handleRequest(PageDao &pageDao, std::string pagename, const Request &r) override;
	~HandlerPageView() override
	{
	}
	using HandlerPage::HandlerPage;
};

#endif // HANDLERPAGEVIEW_H
