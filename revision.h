#ifndef REVISION_H
#define REVISION_H
#include <string>
#include <ctime>
class Revision
{
  public:
	unsigned int revision;
	time_t timestamp;
	std::string comment;
	std::string page;
	std::string author;
	std::string content;

	Revision();
};

#endif // REVISION_H
