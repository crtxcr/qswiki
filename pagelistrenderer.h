#ifndef PAGELISTRENDERER_H
#define PAGELISTRENDERER_H
#include "utils.h"
#include "page.h"
#include "template.h"
#include "htmllink.h"
#include "urlprovider.h"
#include "database/database.h"

class PageListRenderer
{
private:
	Template *templ;
	UrlProvider *urlProvider;
	Database *database;

public:
	PageListRenderer(Template &templ, UrlProvider &provider, Database &database);

	std::string render(const std::vector<Page> &pages, std::string type) const;

	inline static const std::string RENDER_GROUP_BY_LETTER { "letter" };
	inline static const std::string RENDER_GROUP_BY_CREATIONDATE { "creationdate" };


};
#endif
