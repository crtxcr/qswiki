#ifndef CLICONSOLE_H
#define CLICONSOLE_H

#include <iostream>
#include <string>
#include <vector>
#include <sys/socket.h>
#include <sys/un.h>
#include "cli.h"

class CLIConsole
{
  private:
	struct sockaddr_un server;
	int sock;
	CLIHandler *handler;
	std::string socketPath;
	bool attached = false;
	std::pair<bool, std::string> send(std::string input);
	void attach();

  public:
	CLIConsole(CLIHandler &cliHandler, std::string socketPath);
	void startInteractive();
};

#endif // CLICONSOLE_H
