#ifndef HEADLINE_H
#define HEADLINE_H

#include <string>
class Headline
{
  public:
	unsigned int level;
	std::string title;
};

#endif // HEADLINE_H
