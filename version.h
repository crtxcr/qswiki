#ifndef VERSION_H
#define VERSION_H

#include <string>
std::string git_commit_id();
std::string get_version_string();
#endif // VERSION_H
