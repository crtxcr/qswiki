#ifndef DYNAMICCONTENTPOSTLIST_H
#define DYNAMICCONTENTPOSTLIST_H

#include "dynamiccontent.h"
class DynamicContentPostList : public DynamicContent
{
  public:
	using DynamicContent::DynamicContent;
	std::string render() override;
};

#endif // DYNAMICCONTENTPOSTLIST_H
