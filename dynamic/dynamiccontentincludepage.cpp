#include "dynamiccontentincludepage.h"
#include "../parser.h"
std::string DynamicContentIncludePage::render()
{
	auto revisionDao = this->database->createRevisionDao();
	auto rev = revisionDao->getCurrentForPage(this->argument);
	if(rev)
	{
		/* Quite dirty */
		if(rev->content.find("[cmd:allowinclude]1[") != std::string::npos)
		{
			return rev->content;
		}
	}
	return {};
}
