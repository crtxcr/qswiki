#ifndef DYNAMICCONTENTFACTORY_H
#define DYNAMICCONTENTFACTORY_H

#include "dynamiccontent.h"

class DynamicContentFactory
{
private:
	Template *templ;
	Database *db;
	UrlProvider *urlProvider;
	Session *session;
	
public:
	DynamicContentFactory(Template &templ, Database &db, UrlProvider &urlProvider, Session &session)
	{
		this->templ = &templ;
		this->db = &db;
		this->urlProvider = &urlProvider;
		this->session = &session;
	}
	
	template <class T> inline std::shared_ptr<T> createDynamicContent()
	{
		return std::make_shared<T>(*this->templ, *this->db, *this->urlProvider, *this->session);
	}
	
	
};
#endif // DYNAMICCONTENTFACTORY_H_INCLUDED
