#ifndef DYNAMICCONTENT_H
#define DYNAMICCONTENT_H
#include <string>
#include "../database/database.h"
#include "../template.h"
#include "../urlprovider.h"
class DynamicContent
{
  protected:
	Template *templ;
	Database *database;
	UrlProvider *urlProvider;
	Session *userSession;

	std::string argument;

  public:
	DynamicContent(Template &templ, Database &database, UrlProvider &urlProvider, Session &session);
	virtual std::string render() = 0;
	virtual void setArgument(std::string argument)
	{
		this->argument = argument;
	}
	virtual ~DynamicContent()
	{
	}
};

#endif // DYNAMICCONTENT_H
