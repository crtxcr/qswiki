#ifndef DYNAMICPOSTRENDERER_H
#define DYNAMICPOSTRENDERER_H
#include "dynamiccontent.h"
class DynamicPostRenderer : public DynamicContent
{
private:
	std::string category;
	std::string customlinkurl;
	std::string templatepartname = "dynamic/categoryrendererentry";

  public:
	using DynamicContent::DynamicContent;
	std::string render() override;
	void setArgument(std::string argument) override;
	std::string linkToPage(std::string page);
};

#endif // DYNAMICPOSTRENDERER_H
