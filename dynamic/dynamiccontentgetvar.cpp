#include "dynamiccontentgetvar.h"

std::string DynamicContentGetVar::render()
{
	return (*this->map)[this->argument];
}

void DynamicContentGetVar::setMap(std::map<std::string, std::string> &map)
{
	this->map = &map;
}
