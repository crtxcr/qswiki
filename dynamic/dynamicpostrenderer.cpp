#include <chrono>
#include "dynamicpostrenderer.h"
#include "../parser.h"
#include "../utils.h"

void DynamicPostRenderer::setArgument(std::string argument)
{
	auto splitted = utils::split(argument, '|');
	this->category = splitted[0];
	if(splitted.size() >= 2)
	{
		this->templatepartname = splitted[1];
	}
	if(splitted.size() >= 3)
	{
		this->customlinkurl = splitted[2];
	}
}

std::string DynamicPostRenderer::linkToPage(std::string page)
{
	if(this->customlinkurl.empty())
	{
		return this->urlProvider->page(page);
	}
	return utils::strreplace(this->customlinkurl, "{page}", page);
}

std::string DynamicPostRenderer::render()
{
	auto categoryDao = this->database->createCategoryDao();
	auto pageDao = this->database->createPageDao();
	auto revisionDao = this->database->createRevisionDao();
	auto permissionDao = this->database->createPermissionsDao();

	QueryOption option;
	option.includeUnlisted = true;
	auto members = categoryDao->fetchMembers(this->category, option);
	std::vector<std::pair<std::string, time_t>> pageList;
	for(const Page &member : members)
	{
		Permissions perms = permissionDao->find(member.name, this->userSession->user.login)
								.value_or(this->userSession->user.permissions);
		if(perms.canRead())
		{
			auto revision = revisionDao->getRevisionForPage(member.name, 1);
			pageList.push_back({member.name, revision->timestamp});
		}
	}
	std::sort(pageList.begin(), pageList.end(),
			  [](std::pair<std::string, time_t> &a, std::pair<std::string, time_t> &b) { return a.second > b.second; });

	std::string entry = this->templ->loadResolvedPart(this->templatepartname);
	std::stringstream stream;
	for(auto &pair : pageList)
	{
		std::optional<Revision> revision = revisionDao->getCurrentForPage(pair.first);
		if(revision)
		{
			std::string link = linkToPage(pair.first);
			Parser parser;

			std::string date = utils::toISODateTime(revision->timestamp);
			Varreplacer replacer{"{"};
			replacer.addKeyValue("url", link);
			replacer.addKeyValue("date", date);
			replacer.addKeyValue("content", parser.parse(*pageDao, *this->urlProvider,
														 parser.extractFirstTag("content", revision->content)));
			stream << replacer.parse(entry);
		}
	}
	return stream.str();
}
