#ifndef DYNAMICCONTENTINCLUDEPAGE_H
#define DYNAMICCONTENTINCLUDEPAGE_H
#include "dynamiccontent.h"
class DynamicContentIncludePage : public DynamicContent
{
  public:
	using DynamicContent::DynamicContent;

	std::string render();
};

#endif // DYNAMICCONTENTINCLUDEPAGE_H
