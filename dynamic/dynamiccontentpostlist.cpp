#include <chrono>
#include "dynamiccontentpostlist.h"

std::string DynamicContentPostList::render()
{
	auto categoryDao = this->database->createCategoryDao();
	auto pageDao = this->database->createPageDao();
	auto revisionDao = this->database->createRevisionDao();
	auto permissionDao = this->database->createPermissionsDao();

	QueryOption option;
	option.includeUnlisted = false;
	auto members = categoryDao->fetchMembers(this->argument, option);
	std::vector<std::pair<std::string, time_t>> pageList;
	for(const Page &member : members)
	{
		Permissions perms = permissionDao->find(member.name, this->userSession->user.login)
								.value_or(this->userSession->user.permissions);
		if(perms.canRead()) /* TODO: Maybe add canList() */
		{
			auto revision = revisionDao->getRevisionForPage(member.name, 1);
			pageList.push_back({member.name, revision->timestamp});
		}
	}
	std::sort(pageList.begin(), pageList.end(),
			  [](std::pair<std::string, time_t> &a, std::pair<std::string, time_t> &b) { return a.second > b.second; });

	std::string postListBegin = this->templ->loadResolvedPart("dynamic/postlistbegin");
	std::string postListEnd = this->templ->loadResolvedPart("dynamic/postlistend");
	std::string postLink = this->templ->loadResolvedPart("dynamic/postlistlink");
	std::stringstream stream;
	stream << postListBegin;
	for(auto &pair : pageList)
	{
		std::string link = this->urlProvider->page(pair.first);
		std::string date = utils::toISODate(pair.second);
		Varreplacer replacer{"{"};
		replacer.addKeyValue("url", link);
		replacer.addKeyValue("date", date);
		replacer.addKeyValue("title", pageDao->find(pair.first)->title);

		stream << replacer.parse(postLink);
	}
	stream << postListEnd;
	return stream.str();
}
