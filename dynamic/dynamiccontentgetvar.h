#ifndef DYNAMICCONTENTGETVAR_H
#define DYNAMICCONTENTGETVAR_H

#include "dynamiccontent.h"
class DynamicContentGetVar : public DynamicContent
{
  private:
	std::map<std::string, std::string> *map;

  public:
	using DynamicContent::DynamicContent;

	// DynamicContent interface
  public:
	std::string render();
	void setMap(std::map<std::string, std::string> &map);
};

#endif // DYNAMICCONTENTGETVAR_H
