#include "dynamiccontentsetvar.h"

std::string DynamicContentSetVar::render()
{
	auto result = utils::split(this->argument, '=');
	if(result.size() == 2)
	{
		this->map->emplace(std::make_pair(result[0], result[1]));
	}
	return {};
}

void DynamicContentSetVar::setArgument(std::string argument)
{
	this->argument = argument;
}

void DynamicContentSetVar::setMap(std::map<std::string, std::string> &map)
{
	this->map = &map;
}
