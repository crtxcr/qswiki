#include "dynamiccontent.h"

DynamicContent::DynamicContent(Template &templ, Database &database, UrlProvider &provider, Session &session)
{
	this->templ = &templ;
	this->database = &database;
	this->urlProvider = &provider;
	this->userSession = &session;
}
