#ifndef DYNAMCCONTENTPUSHVAR_H
#define DYNAMCCONTENTPUSHVAR_H
#include "dynamiccontent.h"
class DynamicContentSetVar : public DynamicContent
{
  private:
	std::map<std::string, std::string> *map;

  public:
	using DynamicContent::DynamicContent;

	std::string render();
	void setArgument(std::string argument);
	void setMap(std::map<std::string, std::string> &map);
};

#endif // DYNAMCCONTENTPUSHVAR_H
