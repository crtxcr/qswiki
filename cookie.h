#ifndef COOKIE_H
#define COOKIE_H

#include <string>
class Cookie
{
  public:
	std::string key;
	std::string value;
	int expires;

	Cookie(const std::string &key, const std::string &val, int expireSeconds = 0);

	std::string createHeaderValue() const
	{
		return key + "=" + value + "; path=/; HttpOnly";
	}
};

#endif // COOKIE_H
