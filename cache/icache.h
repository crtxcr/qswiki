#ifndef ICACHE_H
#define ICACHE_H
#include <string>
#include <string_view>
#include <optional>
#include "../utils.h"
class ICache
{
  public:
	virtual std::optional<std::string> get(std::string_view key) const = 0;
	virtual void put(std::string_view key, std::string val) = 0;
	virtual void remove(std::string_view key) = 0;
	virtual void removePrefix(std::string_view prefix) = 0;
	virtual void clear() = 0;
	virtual ~ICache()
	{
	}
};

#endif // ICACHE_H
