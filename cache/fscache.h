#ifndef FSCACHE_H
#define FSCACHE_H
#include "icache.h"
class FsCache : public ICache
{
  private:
	std::string path;
	std::string getFilePath(std::string_view path) const;

  public:
	FsCache(std::string directory);
	std::optional<std::string> get(std::string_view key) const;
	void put(std::string_view key, std::string val);
	void remove(std::string_view key);
	void removePrefix(std::string_view prefix);
	void clear();
	using ICache::ICache;
	~FsCache()
	{
	}
};

#endif // FSCACHE_H
