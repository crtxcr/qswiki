#include "icache.h"

class NoCache : public ICache
{
public:
	NoCache(std::string p)
	{

	}
	virtual std::optional<std::string> get(std::string_view key) const
	{
		return {};
	}
	virtual void put(std::string_view key, std::string val)
	{
		return;
	}
	virtual void remove(std::string_view key)
	{
		return;
	}
	virtual void removePrefix(std::string_view prefix)
	{
		return;
	}
	virtual void clear()
	{
		return;
	}
};
