/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <iostream>
#include "varreplacer.h"
#include "utils.h"
#include "logger.h"
// TODO: postfix
Varreplacer::Varreplacer(std::string_view prefix)
{
	this->prefix = prefix;
}

std::tuple<std::string_view, std::string_view> Varreplacer::extractKeyAndValue(std::string_view var)
{
	var.remove_prefix(prefix.length());
	size_t colonPos = var.find(':');
	// HACK
	if(colonPos == std::string_view::npos)
	{
		std::string_view key = var;
		std::string_view value = var;
		return std::make_tuple(key, value);
	}
	std::string_view key = var.substr(0, colonPos);
	std::string_view value = var.substr(colonPos + 1);

	return std::make_tuple(key, value);
}
void Varreplacer::addResolver(std::string_view key, std::function<std::string(std::string_view)> resolver)
{
	this->resolverFunctionsMap.insert(std::make_pair(key, resolver));
}

void Varreplacer::addKeyValue(std::string_view key, std::string value)
{
	this->keyValues.insert(std::make_pair(key, value));
}

std::string Varreplacer::makeReplacement(std::string_view varkeyvalue)
{
	std::string_view key;
	std::string_view value;

	std::tie(key, value) = extractKeyAndValue(varkeyvalue);
	if(keyValues.contains(key))
	{
		std::string replacementContent = keyValues[key];
		return replacementContent;
	}
	else if(resolverFunctionsMap.contains(key))
	{

		auto resolver = this->resolverFunctionsMap[key];
		std::string replacementContent = resolver(value);
		return replacementContent;
	}

	return std::string{varkeyvalue} + '}';
}

std::string Varreplacer::parse(std::string_view content)
{
	std::string result;

	size_t pos;
	while((pos = content.find(prefix)) != std::string_view::npos)
	{
		if(pos != 0)
		{
			auto part = content.substr(0, pos);

			result += part;
			content.remove_prefix(pos);
		}
		auto endpos = content.find("}");
		if(endpos == std::string_view::npos)
		{
			throw "misformated";
		}
		std::string_view varkeyvalue = content.substr(0, endpos);
		result += makeReplacement(varkeyvalue);
		content.remove_prefix(endpos + 1);
	}
	result += content;

	return result;
}
