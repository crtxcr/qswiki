#ifndef REVISIONRENDERER_H
#define REVISIONRENDERER_H
#include "revision.h"
#include "templatepage.h"
#include "dynamic/dynamiccontentfactory.h"
#include "iparser.h"
#include "parser.h"
class RevisionRenderer
{
private:
	DynamicContentFactory dynamicContentFactory;
	Database *db;
	UrlProvider *urlProvider;
	std::map<std::string, std::string> dynamicVarsMap;
	
	std::string dynamicCallback(std::string_view key, std::string_view value);
	Parser parser;
	
public:
	RevisionRenderer(Template &templ, Database &db, UrlProvider &urlProvider, Session &session) :dynamicContentFactory(templ, db, urlProvider, session)
	{
		this->db = &db;
		this->urlProvider = &urlProvider;
	}
	std::string renderContent(std::string content);
	std::string renderContent(const Revision &r, std::string_view customTitle);
};

#endif // REVISIONRENDERER_H
