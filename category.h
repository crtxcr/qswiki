#ifndef CATEGORY_H
#define CATEGORY_H

#include <string>
class Category
{
  public:
	Category();
	unsigned int id;
	std::string name;
};

#endif // CATEGORY_H
