#ifndef TEMPLATEPAGE_H
#define TEMPLATEPAGE_H
#include <string>
#include <string_view>
#include <map>
#include <memory>
class TemplatePage
{
  private:
	std::shared_ptr<const std::string> content;
	std::map<std::string, std::string> varsMap;

  public:
	TemplatePage();
	TemplatePage(std::string content);

	std::string render() const;

	void setVar(const std::string &key, std::string value);
};

#endif // TEMPLATEPAGE_H
