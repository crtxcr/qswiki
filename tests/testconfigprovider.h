#ifndef TESTCONFIGPROVIDER_H
#define TESTCONFIGPROVIDER_H
#include "../config.h"

class TestConfigProvider
{
  public:
	TestConfigProvider();

	static Config testConfig();
};

#endif // TESTCONFIGPROVIDER_H
