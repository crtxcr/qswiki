/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <map>
#include <string>
#include <exception>
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "../utils.h"
TEST(Utils, hasKey)
{
	std::map<std::string, int> testmap;
	testmap["test"] = 23;
	testmap["test2"] = 24;
	ASSERT_TRUE(utils::hasKey(testmap, std::string{"test"}));
	ASSERT_TRUE(utils::hasKey(testmap, std::string{"test2"}));
	ASSERT_FALSE(utils::hasKey(testmap, std::string{"testthere"}));
	ASSERT_FALSE(utils::hasKey(testmap, std::string{}));
}

TEST(Utils, urldecode)
{
	std::string testr1 = "abc123=23";
	std::string decoded = utils::urldecode(testr1);
	ASSERT_TRUE(testr1 == decoded);

	std::string testr2 = "a%20b";
	std::string decoded2 = utils::urldecode(testr2);
	std::string expected2 = "a b";
	ASSERT_TRUE(decoded2 == expected2);

	std::string testr3 = "a%";
	std::string expected3 = "a%";

	std::string decoded3 = utils::urldecode(testr3);
	ASSERT_TRUE(testr3 == expected3);
}

TEST(UTILS, toUInt)
{
	EXPECT_NO_THROW({
		std::string number = "23";
		unsigned int expected = 23;
		unsigned int actual = utils::toUInt(number);
		ASSERT_EQ(expected, actual);
	});

	ASSERT_THROW(utils::toUInt("abc"), std::invalid_argument);
	ASSERT_THROW(utils::toUInt("999999999999999999999"), std::out_of_range);
}

TEST(UTILS, html_xss)
{
	std::string input = "<b>";
	std::string escaped = utils::html_xss(input);

	ASSERT_TRUE(escaped.find('<') == std::string::npos);
}

TEST(UTILS, strreplace)
{

	std::string input = "ABCHelloDEF";
	std::string output = utils::strreplace(input, "Hello", "Bye");
	ASSERT_TRUE("ABCByeDEF" == output);

	input = "XXLeaveUsYY";
	output = utils::strreplace(input, "NotFoundInString", "WithSomething");

	ASSERT_TRUE(output == input);

	input = "AA2233";
	output = utils::strreplace(input, "A", "1");

	ASSERT_TRUE(output == "112233");

	input = "someTESTtest";
	output = utils::strreplace(input, "TEST", "TEST");

	ASSERT_TRUE(output == input);
}
