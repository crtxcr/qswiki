/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <map>
#include <string>
#include <exception>
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "../request.h"

TEST(Request, initGetMap)
{
	Request r;
	r.initGetMap("hi=there&bye=here");
	ASSERT_TRUE(r.get("hi") == "there");
	ASSERT_TRUE(r.get("bye") == "here");

	Request r2;
	r2.initGetMap("hi===");
	ASSERT_TRUE(r2.get("hi") == "==");

	Request r3;
	r3.initGetMap("abcdef=aaa&&&&&dddddd=2");
	ASSERT_TRUE(r3.get("abcdef") == "aaa");
	ASSERT_TRUE(r3.get("dddddd") == "2");
	ASSERT_TRUE(r3.get("&") == "");

	Request xss;
	xss.initGetMap("q=\"><b>x</b>\"");

	ASSERT_TRUE(xss.get("q") != "");
	ASSERT_TRUE(xss.get("q").find("<") == std::string::npos);
}

TEST(Request, initCookies)
{
	Request r1;
	r1.initCookies("aaaa=22aa");
	ASSERT_TRUE(r1.cookie("aaaa") == "22aa");
}
