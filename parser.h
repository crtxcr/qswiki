#ifndef PARSER_H
#define PARSER_H
#include "iparser.h"

class Parser : public IParser
{
  private:
	std::string processLink(const PageDao &pageDao, UrlProvider &urlProvider, std::smatch &match) const;
	std::string processImage(std::smatch &match) const;

  public:
	std::string extractFirstTag(std::string tagname, const std::string &content) const override;
	std::string extractCommand(std::string cmdname, const std::string &content) const override;
	std::vector<std::string> extractCommands(std::string cmdname, const std::string &content) const override;
	std::vector<Headline> extractHeadlines(const std::string &content) const override;
	std::vector<std::string> extractCategories(const std::string &content) const override;
	using IParser::parse;
	virtual std::string parse(
		const PageDao &pagedao, UrlProvider &provider, const std::string &content,
		const std::function<std::string(std::string_view, std::string_view)> &callback) const override;
	std::string parseDynamics(
		const std::string &content,
		const std::function<std::string(std::string_view, std::string_view)> &callback) const override;

	using IParser::IParser;
};

#endif // PARSER_H
