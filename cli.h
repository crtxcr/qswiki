#ifndef CLI_H
#define CLI_H
#include <iostream>
#include <string>
#include <vector>
#include "database/database.h"
#include "config.h"

class CLIHandler
{
	struct cmd
	{
		std::string name;
		std::string helptext;
		unsigned int required_args;
		std::vector<cmd> subCommands;
		std::function<std::pair<bool, std::string>(CLIHandler *, const std::vector<std::string> &)> func;
	};

  private:
	Database *db;
	Config *conf;

  protected:
	std::pair<bool, std::string> attach([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> cli_help([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> user_add([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> user_change_pw([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> user_rename([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> user_set_perms([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> user_list([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> user_show([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> page_list([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> pageperms_set_permissions([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> version([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> category_list([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> category_delete([[maybe_unused]] const std::vector<std::string> &args);
	std::pair<bool, std::string> category_show([[maybe_unused]] const std::vector<std::string> &args);

	std::vector<struct cmd> cmds{
		{{"user",
		  "user operations on the database",
		  1,
		  {{{"add", "[user] [password] - creates a user", 2, {}, &CLIHandler::user_add},
			{"changepw", "[user] [password] - changes the password of user", 2, {}, &CLIHandler::user_change_pw},
			{"rename", "[user] [new name] - renames a user", 2, {}, &CLIHandler::user_rename},
			{"setperms", "[user] [perms] - sets the permissions of the user", 2, {}, &CLIHandler::user_set_perms},
			{"list", "- lists users", 0, {}, &CLIHandler::user_list},
			{"show", "[user] - show detailed information about user", 1, {}, &CLIHandler::user_show}}},
		  &CLIHandler::cli_help},
		 {"page",
		  "operation on pages",
		  1,
		  {{{"list", "- lists existing pages", 0, {}, &CLIHandler::page_list}}},
		  &CLIHandler::cli_help},
		 {"category",
		  "operation on categories",
		  1,
		  {{{"list", "- lists existing categories", 0, {}, &CLIHandler::category_list},
			{"delete", " - deletes a category", 1, {}, &CLIHandler::category_delete},
			{"show", " - shows pages of a category", 1, {}, &CLIHandler::category_show}}},
		  &CLIHandler::cli_help},
		 {"pageperms",
		  "set permissions on pages",
		  1,
		  {{{"set",
			 "- [page] [username] [permissions] set permisisons on page",
			 3,
			 {},
			 &CLIHandler::pageperms_set_permissions}}},
		  &CLIHandler::cli_help},
		 {"exit",
		  "exit cli",
		  0,
		  {},
		  [](CLIHandler *, [[maybe_unused]] const std::vector<std::string> &args) -> std::pair<bool, std::string>
		  {
			  exit(EXIT_SUCCESS);
			  return {true, ""};
		  }},
		 {"help", "print this help", 0, {}, &CLIHandler::cli_help},
		 {"attach", "attach to running instance", 0, {}, &CLIHandler::attach},
		 {"version", "print verison info", 0, {}, &CLIHandler::version}}};

	std::pair<bool, std::string> processCommand(const std::vector<CLIHandler::cmd> &commands, std::string cmd,
												const std::vector<std::string> &args);

  public:
	CLIHandler(Config &config, Database &d);
	std::pair<bool, std::string> processCommand(std::string cmd, const std::vector<std::string> &args);
	static std::pair<std::string, std::vector<std::string>> splitCommand(std::string input);
};

#endif // CLI_H
