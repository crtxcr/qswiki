#ifndef AUTHENTICATOR_H
#define AUTHENTICATOR_H
#include <variant>
#include "database/userdao.h"

#define AUTH_DEFAULT_SALT_SIZE 32
enum AuthenticationError
{
	UserNotFound,
	UserDisabled,
	PasswordNotMatch,
	BannedIP,
	GeneralError
};

class Authenticator
{
  private:
	UserDao *userDao;
	bool isBanned(std::string ip);
	void incFailureCount(std::string ip);
	std::vector<char> pbkdf5(std::string password, const std::vector<char> &salt) const;

  public:
	Authenticator(UserDao &userDao);
	std::variant<User, AuthenticationError> authenticate(std::string username, std::string password);
	std::variant<User, AuthenticationError> authenticate(std::string username, std::string password, std::string ip);
	std::vector<char> hash(std::string password, const std::vector<char> &salt);
};

#endif // AUTHENTICATOR_H
