#ifndef SESSION_H
#define SESSION_H
#include <ctime>
#include "user.h"
class Session
{
  public:
	Session()
	{
	}
	Session(User u);
	bool loggedIn;
	User user;
	std::string token;
	std::string csrf_token;
	time_t creation_time;

	static Session createAnon();
};

#endif
