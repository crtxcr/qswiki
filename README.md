# qswiki

## About
qswiki is a wiki software, intended for my needs. Originally  implemented in C, it's now written in C++.

## Dude... why?

tl;dr: It was a playground, an experiment (taken too far). I guess at some point I couldn't stop, because I've already
started.

### History
Several years ago, I wanted to setup a personal wiki on my raspberry
pi. However, the distribution I used back then did not have a PHP package
for ARM. So instead of switching distributions or searching for other
wikis that I could use, I simply decided I would write one in C. Yes,
that's an odd way  to approach the problem and indeed, I may have had too
much time back  then. Also, I wanted to see how it's like to write a
"web app" in C and wanted to sharpen my C skills a little bit.

Of course, it's pretty straightforward at first. No really: Just use CGI
and print your HTML to stdout.And indeed, that would have been more than enough for my use cases.

But then I decided to play around and started using FastCGI (with the official
library from now  defunct fastcgi.com) and created a multi-threaded version.
It initially  used a "pile of files database", but that became too painful,
so then I started using sqlite.

C++
---
Eventually, since it was mostly a playground for me, the code became
unmaintainable. Furthermore, I initially wanted something quick and given that
it was CGI, I didn't bother taking care of memory leaks.
After initiating a FastCGI interface, they became an issue and then the
task of avoiding memory leaks became too annoying. And of course, C does n
ot include any "batteries" and while I could manage, this too was another
good reason.

Overall, I am just continuing the experiment with >=C++17 now. It's not
nearly as bad as you would expect perhaps. Some things are surprisingly
convenient even. Still, the standard library is lacking and
I would hope for a some better built-in Unicode support in future C++
standards.


## Features
Some essential features are lacking, such as a diff between revisions,
user registration UI, etc.

It doesn't compete with any other software anyway.

 - CGI
 - HTTP server using the header only library [cpp-httplib](https://github.com/yhirose/cpp-httplib). It's more
 portable and more "future-proof" than FastCGI (since the official website
 disappeared, the library's future appears to be uncertain).
 - Support for user accounts. Passwords are stored using PBKDF2.
  sqlite database, but not too much of an effort to add other types of
  storage backends. sqlite is using the great header only library
  [sqlite_modern_cpp](https://github.com/SqliteModernCpp)
 - Relatively fine-grained permission system.
 - Categories
 - Templates
 - FTS search
 - Caching
 - Blog-like functionality
 - RSS/Atom feeds

## Security
[exile.h](https://github.com/quitesimpleorg/exile.h) is used
to restrict access to the files the wiki needs.  It doesn't have access to other paths
in the system and the system calls that the qswiki process can make are restricted.

As for "web security", all POST requests are centrally protected against CSRF attacks and all input is escaped against XSS
attacks.

## Building
Dependencies:
  - cpp-httplib: https://github.com/yhirose/cpp-httplib
  - SqliteModernCpp: https://github.com/SqliteModernCpp
  - exile.h: https://gitea.quitesimple.org/crtxcr/exile.h
  - sqlite3: https://sqlite.org/index.html

The first three are header-only libraries that are included as a git submodule. The others must
be installed, e. g. by using your distributions standard method.

If all dependencies are available, run:
```
  git submodule init
  git submodule update
  make release
```

Setup
=====
To be written
