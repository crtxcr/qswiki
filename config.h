#ifndef CONFIG_H
#define CONFIG_H
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include "permissions.h"
#include "utils.h"

/* Stuff handlers/ (may) need to know */
struct HandlerConfig
{
	Permissions anon_permissions;
	std::string wikiname;
	std::string anon_username;
	std::string page_title_template;
	int max_pagename_length;
	int query_limit;
};

struct ConfigUrls
{
	std::string linkindex;
	std::string linkrecent;
	std::string linkallpages;
	std::string linkallpagesrendertype;
	std::string linkallcats;
	std::string linkshere;
	std::string linkpage;
	std::string linkpagebytitle;
	std::string linkrevision;
	std::string linkhistory;
	std::string linkedit;
	std::string linksettings;
	std::string linkdelete;
	std::string linklogout;
	std::string linkcategory;
	std::string linkcategoryrendertype;
	std::string loginurl;
	std::string linkrecentsort;
	std::string actionurl;
	std::string settingsurl;
	std::string deletionurl;
	std::string linkhistorysort;
	std::string adminregisterurl;
	std::string usersettingsurl;
	std::string rooturl;
	std::string atomurl;
};

class ConfigVariableResolver
{
  private:
	const std::map<std::string, std::string> *configmap;

  public:
	ConfigVariableResolver()
	{
	}

	ConfigVariableResolver(const std::map<std::string, std::string> &configmap)
	{
		this->configmap = &configmap;
	}

	std::string getConfig(const std::string &key) const
	{
		return utils::getKeyOrEmpty(*configmap, key);
	}
};

class Config
{
  private:
	std::map<std::string, std::string> configmap;
	std::string required(const std::string &key);

	std::string optional(const std::string &key, std::string defaultvalue = "");
	int optional(const std::string &key, int defaulvalue);
	uint64_t optional(const std::string &key, uint64_t defaultvalue);

  public:
	Config(const std::map<std::string, std::string> &map);

	ConfigUrls urls;
	ConfigVariableResolver configVarResolver;
	HandlerConfig handlersConfig;

	std::string wikipath;
	std::string templatepath;
	std::string templateprefix;
	std::string logfile;
	std::string connectionstring;
	int session_max_lifetime;
	int threadscount;

	uint64_t max_payload_length;
};

class ConfigReader
{
  private:
	std::string path;

  public:
	ConfigReader(const std::string &file);
	Config readConfig();
};

#endif // CONFIG_H
