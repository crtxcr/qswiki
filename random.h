#ifndef RANDOM_H
#define RANDOM_H
#include <unistd.h>
#include <sys/syscall.h>
#ifdef __linux__
#include <sys/random.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string>

#if __OpenBSD__
inline int getrandom(void *buf, size_t buflen, unsigned int flags)
{
	arc4random_buf(buf, buflen);
	return 0;
}
#endif
/* TODO: if the >=C++11 prngr are good enough, use them */
class Random
{
  public:
	Random();
	std::string getRandomHexString(unsigned int bytes) const;
	std::vector<char> getRandom(unsigned int bytes) const;
};

#endif // RANDOM_H
