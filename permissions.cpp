/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "permissions.h"

static const std::map<std::string, int> permmap = {{"can_nothing", PERM_CAN_NOTHING},
												   {"can_read", PERM_CAN_READ},
												   {"can_edit", PERM_CAN_EDIT},
												   {"can_page_history", PERM_CAN_PAGE_HISTORY},
												   {"can_global_history", PERM_CAN_GLOBAL_HISTORY},
												   {"can_delete", PERM_CAN_DELETE},
												   {"can_see_page_list", PERM_CAN_SEE_PAGE_LIST},
												   {"can_create", PERM_CAN_CREATE},
												   {"can_see_category_list", PERM_CAN_SEE_CATEGORY_LIST},
												   {"can_see_links_here", PERM_CAN_SEE_LINKS_HERE},
												   {"can_search", PERM_CAN_SEARCH},
												   {"can_set_page_perms", PERM_CAN_SET_PAGE_PERMS}};

Permissions::Permissions(int permissions)
{
	this->permissions = permissions;
}

Permissions::Permissions(const std::string &str)
{
	this->permissions = 0;
	for(auto permission : permmap)
	{
		if(str.find(permission.first) != std::string::npos)
		{
			this->permissions |= permission.second;
		}
	}
}

std::string Permissions::toString(int perms)
{
	std::string result;
	for(auto pair : permmap)
	{
		if(pair.second & perms)
		{
			result += pair.first + ",";
		}
	}
	if(result.size() > 0)
	{
		result.pop_back();
	}
	return result;
}
