/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <filesystem>
#include "template.h"
#include "varreplacer.h"
#include "urlprovider.h"
#include "htmllink.h"
#include "logger.h"
Template::Template(std::string templateprefix, std::string templatepath, ConfigUrls &configUrls,
				   ConfigVariableResolver &configVarsResolver, MapCache<TemplatePage> &pageCache)
{
	this->templateprefix = templateprefix;
	this->templatepath = templatepath;
	this->configUrls = &configUrls;
	this->configVarResolver = &configVarsResolver;
	this->pageCache = &pageCache;
}

TemplatePage Template::getPage(const std::string &pagename)
{
	auto result = this->pageCache->find(pagename);
	if(result)
	{
		return *result;
	}
	auto page = createPage(pagename);
	this->pageCache->set(pagename, page);
	return page;
}

std::string Template::getPartPath(std::string_view partname)
{
	auto absolute_path = std::filesystem::canonical(std::filesystem::path{this->templatepath} / partname);
	std::string result = absolute_path.string();
	if(result.starts_with(this->templatepath))
	{
		return result;
	}
	return "";
}

std::string Template::loadPartContent(std::string_view partname)
{
	std::string partpath = getPartPath(partname);
	return utils::readCompleteFile(partpath);
}
std::string Template::loadResolvedPart(std::string_view partname)
{
	return resolveIncludes(loadPartContent(partname));
}

std::string Template::resolveIncludes(std::string_view content)
{
	Varreplacer replacer(this->templateprefix);
	replacer.addResolver("include", [&](std::string_view key) { return loadResolvedPart(key); });
	return replacer.parse(content);
}

TemplatePage Template::createPage(std::string_view name)
{
	std::string content = loadResolvedPart(name);
	Varreplacer replacer(this->templateprefix);
	replacer.addResolver("config",
						 [&](std::string_view key) { return this->configVarResolver->getConfig(std::string(key)); });
	// TODO: Varreplacer is not recursive, but since includes might add new vars, it may not be this bad anyway.
	//
	return TemplatePage(replacer.parse(content));
}

// TODO: this restricts template a bit
std::string Template::renderSearch(const std::vector<std::string> &results,
								   std::function<std::string(std::string)> callback) const
{
	HtmlLink link;

	std::string result;
	char lastchar = 0;
	for(const std::string &str : results)
	{
		int upper = toupper(str[0]); // TODO: this is not unicode safe.
		if(lastchar != upper)
		{
			lastchar = upper;

			result += std::string("<div class=\"letter_search_result\">") + lastchar + std::string("</div>");
		}
		link.href = callback(str);
		link.innervalue = str;
		result += link.render() + "<br>";
	}
	return result;
}
std::string Template::renderSearch(const std::vector<std::string> &results) const
{
	UrlProvider urlprovider(*this->configUrls);
	return renderSearch(results, [&urlprovider](std::string s) { return urlprovider.page(s); });
}
std::string Template::renderSearch(const std::vector<SearchResult> &results) const
{
	UrlProvider urlprovider(*this->configUrls);
	HtmlLink link;
	char lastchar = 0;
	std::string result;
	for(const SearchResult &sr : results)
	{
		int upper = toupper(sr.pagename[0]); // TODO: this is not unicode safe.
		if(lastchar != upper)
		{
			lastchar = upper;

			result += std::string("<div class=\"letter_search_result\">") + lastchar + std::string("</div>");
		}
		link.href = urlprovider.page(sr.pagename);
		link.innervalue = sr.pagename;
		result += link.render() + "<br>";
	}
	return result;
}

std::string Template::renderRevisionList(const std::vector<Revision> &revisions, bool withpage) const
{
	std::stringstream stream;
	UrlProvider urlprovider(*this->configUrls);

	auto genwithoutpage = [&]
	{
		for(const Revision &revision : revisions)
		{
			stream << "<tr><td><a href=\"" << urlprovider.pageRevision(revision.page, revision.revision) << "\">"
				   << revision.revision << "</a></td>"
				   << "<td>" << revision.author << "</td>"
				   << "<td>" << revision.comment << "</td>"
				   << "<td>" << utils::toISODateTime(revision.timestamp) << "</td></tr>";
		}
	};

	auto genwithpage = [&]
	{
		for(const Revision &revision : revisions)
		{

			stream << "<tr><td><a href=\"" << urlprovider.pageRevision(revision.page, revision.revision) << "\">"
				   << revision.page << "</a></td>"
				   << "<td>" << revision.revision << "</td>"
				   << "<td>" << revision.author << "</td>"
				   << "<td>" << revision.comment << "</td>"
				   << "<td>" << utils::toISODateTime(revision.timestamp) << "</td></tr>";
		}
	};

	if(withpage)
	{

		genwithpage();
	}
	else
	{
		genwithoutpage();
	}

	return stream.str();
}
