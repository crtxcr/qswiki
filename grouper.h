#include "utils.h"

template<class G, class V, class C>
class Grouper
{
	std::map<G, std::vector<const V*>, C> results;
public:

	Grouper(C c)
	{
		results = std::map<G, std::vector<const V*>, C>(c);
	}

	void group(const std::function<G(const V&)> &map, const std::vector<V> &values)
	{
		for(const V &v : values)
		{
			results[map(v)].push_back(&v);
		}
	}

	std::map<G, std::vector<const V*>, C> &getResults()
	{
		return this->results;
	}
};
