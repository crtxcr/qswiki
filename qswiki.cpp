/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <iostream>
#include <fstream>
#include <exception>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <filesystem>
#include <getopt.h>
#include "gateway/gatewayinterface.h"
#include "gateway/gatewayfactory.h"
#include "handlers/handlerfactory.h"
#include "database/databasefactory.h"
#include "config.h"
#include "session.h"
#include "template.h"
#include "logger.h"
#include "urlprovider.h"
#include "requestworker.h"
#include "cache/fscache.h"
#include "cache/nocache.h"
#include "sandbox/sandboxfactory.h"
#include "cli.h"
#include "cliconsole.h"
#include "cliserver.h"
#include "version.h"

void sigterm_handler([[maybe_unused]] int arg)
{
	// TODO: proper shutdown.
	exit(EXIT_SUCCESS);
}

void setup_signal_handlers()
{
	struct sigaction sigtermaction;
	sigtermaction.sa_handler = &sigterm_handler;

	int ret = sigaction(SIGTERM, &sigtermaction, NULL);
	if(ret == -1)
	{
		perror("sigaction");
		exit(EXIT_FAILURE);
	}
}

#define OPT_PRINT_VERSION 23

static struct option long_options[] = {{"cli", no_argument, 0, 'c'}, {"version", no_argument, 0, OPT_PRINT_VERSION}};

std::unique_ptr<ICache> createCache(const ConfigVariableResolver &resolver)
{
	std::string path = resolver.getConfig("cache_fs_dir");
	if(path == "")
	{
		return std::make_unique<StringCache>();
	}
	return std::make_unique<FsCache>(path);
}

std::thread background_worker;
void start_background_worker(Database &database, Config &config)
{
	background_worker = std::thread(
		[&database, &config]()
		{
			while(true)
			{
				Logger::log() << "Executing background worker";

				auto sessionDao = database.createSessionDao();
				auto sessionList = sessionDao->fetch();
				time_t now = time(NULL);

				for(Session &sess : sessionList)
				{
					if(now - sess.creation_time > config.session_max_lifetime)
					{
						sessionDao->deleteSession(sess.token);
					}
				}

				std::this_thread::sleep_for(std::chrono::hours(1));
			}
		});
}

int main(int argc, char **argv)
{

	char *configfilepath = NULL;
	int option;
	int option_index;
	bool cli_mode = false;

	if(geteuid() == 0)
	{
		std::cerr << "Do not run this as root!" << std::endl;
		return 1;
	}

	while((option = getopt_long(argc, argv, "cv", long_options, &option_index)) != -1)
	{
		switch(option)
		{
		case 'c':
			cli_mode = true;
			break;
		case OPT_PRINT_VERSION:
			std::cout << get_version_string() << std::endl;
			exit(EXIT_SUCCESS);
			break;
		}
	}

	if(optind == argc)
	{
		std::cerr << "Missing config path" << std::endl;
		return 1;
	}

	configfilepath = argv[optind++];

	auto sandbox = createSandbox();
	// TODO: do we want to keep it mandatory or configurable?
	if(!sandbox->supported())
	{
		Logger::error() << "Sandbox is not supported, exiting";
		exit(EXIT_FAILURE);
	}
	if(argc < 2)
	{
		std::cerr << "no path to config file provided" << std::endl;
		return 1;
	}
	std::string configpath = std::filesystem::absolute(configfilepath).string();

	try
	{
		ConfigReader configreader(configpath);
		Config config = configreader.readConfig();

		setup_signal_handlers();

		std::fstream logstream;
		logstream.open(config.logfile, std::fstream::out | std::fstream::app);
		Logger::setStream(&logstream);

		auto database = createDatabase(config);

		std::string socketPath = config.configVarResolver.getConfig("socketpath");
		CLIHandler cliHandler(config, *database);

		if(cli_mode)
		{
			CLIConsole console{cliHandler, socketPath};
			console.startInteractive();
			return 0;
		}

		// TODO: config.connectiontring only works as long as we only support sqlite of course
		if(!sandbox->enable({
			   config.configVarResolver.getConfig("cache_fs_dir"),
			   config.templatepath,
			   std::filesystem::path(config.logfile).parent_path(),
			   std::filesystem::path(config.connectionstring).parent_path(),
		   }))
		{
			Logger::error() << "Sandboxing for worker could not be enabled!";
			exit(EXIT_FAILURE);
		}

		start_background_worker(*database.get(), config);

		CLIServer cliServer{cliHandler};
		if(!cliServer.detachServer(socketPath))
		{
			Logger::error() << "Error: Failed to detach unix socket server";
			return 1;
		}

		// TODO: quite ugly, anon-handling must be rethought
		auto userdao = database->createUserDao();
		std::optional<User> anon = userdao->find(config.handlersConfig.anon_username);
		if(!anon)
		{
			throw std::runtime_error("No such anon user in database");
		}
		if(anon->enabled)
		{
			throw std::runtime_error("Anon user cannot be enabled");
		}
		anon->permissions = config.handlersConfig.anon_permissions;
		userdao->save(anon.value());
		User::setAnon(anon.value());

		MapCache<TemplatePage> mapCache;
		Template siteTemplate{config.templateprefix, config.templatepath, config.urls, config.configVarResolver,
							  mapCache};
		UrlProvider urlProvider{config.urls};

		auto cache = createCache(config.configVarResolver);
		cache->clear();

		HandlerFactory handlerFactory{config.handlersConfig, siteTemplate, *database.get(), urlProvider, *cache.get()};
		RequestWorker requestWorker{handlerFactory, database->createSessionDao(), siteTemplate};

		auto interface = createGateway(config);

		interface->work(requestWorker);
	}
	catch(const std::exception &e)
	{
		Logger::error() << e.what();
		std::cerr << e.what() << std::endl;
	}
	return 0;
}
