#ifndef REQUESTWORKER_H
#define REQUESTWORKER_H

#include "request.h"
#include "response.h"
#include "session.h"
#include "template.h"
#include "database/database.h"
#include "urlprovider.h"
#include "database/sessiondao.h"
#include "cache/fscache.h"
#include "handlers/handlerfactory.h"

class RequestWorker
{
	Template *templ;
	UrlProvider *urlProvider;
	ICache *cache;
	HandlerFactory *handlerFactory;
	std::unique_ptr<SessionDao> sessionDao;

  private:
	Session retrieveSession(std::string token) const;

  public:
	RequestWorker(HandlerFactory &handlerFactory, std::unique_ptr<SessionDao> sessionDao, Template &templ)
	{
		this->handlerFactory = &handlerFactory;
		this->templ = &templ;
		this->sessionDao = std::move(sessionDao);
	}

	Response processRequest(const Request &r);
};

#endif // REQUESTWORKER_H
