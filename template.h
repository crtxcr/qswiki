#ifndef TEMPLATE_H
#define TEMPLATE_H
#include <functional>
#include <string_view>
#include "config.h"
#include "templatepage.h"
#include "utils.h"
#include "response.h"
#include "searchresult.h"
#include "revision.h"
#include "cache/mapcache.h"
class Template
{
  private:
	ConfigVariableResolver *configVarResolver;
	ConfigUrls *configUrls;
	MapCache<TemplatePage> *pageCache;

	std::string templateprefix;
	std::string templatepath;

	std::string resolveIncludes(std::string_view content);

	std::string getPartPath(std::string_view partname);
	std::string loadPartContent(std::string_view partname);
	TemplatePage createPage(std::string_view name);

  public:
	Template(std::string templateprefix, std::string templatepath, ConfigUrls &configUrls,
			 ConfigVariableResolver &configVarsResolver, MapCache<TemplatePage> &pageCache);

	TemplatePage getPage(const std::string &pagename);
	std::string loadResolvedPart(std::string_view partname);

	std::string renderSearch(const std::vector<std::string> &results,
							 std::function<std::string(std::string)> callback) const;
	std::string renderSearch(const std::vector<std::string> &results) const;
	std::string renderSearch(const std::vector<SearchResult> &results) const;
	std::string renderRevisionList(const std::vector<Revision> &revisions, bool withpage = false) const;
};

#endif // TEMPLATE_H
