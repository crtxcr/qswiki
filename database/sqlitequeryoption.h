#ifndef SQLITEQUERYOPTION_H
#define SQLITEQUERYOPTION_H
#include <string>
#include "queryoption.h"

class SqliteQueryOption
{
  private:
	QueryOption o;
	std::string listedColumnName;
	std::string orderByColumnName;

	bool prependWhere;

  public:
	SqliteQueryOption(const QueryOption &o);

	SqliteQueryOption &setOrderByColumn(std::string name);

	SqliteQueryOption &setListedColumnName(std::string name);

	SqliteQueryOption &setPrependWhere(bool b);

	std::string build();
};

#endif // SQLITEQUERYOPTION_H
