/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <string>
#include <vector>
#include "categorydaosqlite.h"
#include "sqlitequeryoption.h"
CategoryDaoSqlite::CategoryDaoSqlite()
{
}

std::optional<Category> CategoryDaoSqlite::find(std::string name)
{
	try
	{
		Category result;
		*db << "SELECT id, name FROM category WHERE name = ?" << name >> std::tie(result.id, result.name);
		return result;
	}
	catch(const sqlite::exceptions::no_rows &e)
	{
		return {};
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return {};
}

void CategoryDaoSqlite::save(const Category &c)
{

	try
	{
		*db << "INSERT OR IGNORE INTO category (id, name) VALUES (SELECT id FROM category WHERE lower(name) = "
			   "lower(?), lower(?)"
			<< c.name << c.name;
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
}

void CategoryDaoSqlite::deleteCategory(std::string name)
{
	try
	{

		*db << "BEGIN;";
		*db << "DELETE FROM categorymember WHERE category = (SELECT id FROM category WHERE name = ?);" << name;
		*db << "DELETE FROM category WHERE name = ?;" << name;
		*db << "COMMIT;";
	}
	catch(sqlite::sqlite_exception &e)
	{
		*db << "ROLLBACK";
		throwFrom(e);
	}
}

std::vector<std::string> CategoryDaoSqlite::fetchList(QueryOption o)
{
	std::vector<std::string> result;
	try
	{
		auto queryoption = SqliteQueryOption(o).setPrependWhere(true).setOrderByColumn("name").build();
		*db << "SELECT name FROM category " + queryoption >> [&](std::string n) { result.push_back(n); };
	}
	catch(const sqlite::exceptions::no_rows &e)
	{
		return result;
	}
	catch(const sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return result;
}

std::vector<Page> CategoryDaoSqlite::fetchMembers(std::string name, QueryOption o)
{
	std::vector<Page> result;

	SqliteQueryOption queryOption{o};
	std::string queryoptions =
		queryOption.setOrderByColumn("name").setListedColumnName("page.listed").setPrependWhere(false).build();

	try
	{
		auto query =
			*db
			<< "SELECT page.id, page.name AS name, page.title, page.lastrevision, page.listed, page.feedlisted FROM "
			   "categorymember INNER JOIN page ON page.id = "
			   "categorymember.page WHERE category = (SELECT id FROM category WHERE name = ? ) AND " +
				   queryoptions
			<< name;
		query >> [&](unsigned int id, std::string name, std::string title, unsigned int lastrevision, bool listed,
					 bool feedlisted)
		{
			Page p;
			p.name = name;
			p.pageid = id;
			p.title = title;
			p.current_revision = lastrevision;
			p.listed = listed;
			p.feedlisted = feedlisted;
			result.push_back(p);
		};
	}
	catch(const sqlite::exceptions::no_rows &e)
	{
		return result;
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}

	return result;
}
