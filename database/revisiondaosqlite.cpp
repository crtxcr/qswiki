/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "revisiondaosqlite.h"
#include "exceptions.h"
#include "sqlitequeryoption.h"
#include "../utils.h"
RevisionDaoSqlite::RevisionDaoSqlite()
{
}

void RevisionDaoSqlite::save(const Revision &revision)
{
	try
	{
		*db << "savepoint revisionsubmit;";
		*db << "INSERT INTO revision(author, comment, content, creationtime, page, revisionid) VALUES((SELECT id FROM "
			   "user WHERE username = ?), ?, ?, DATETIME(), (SELECT id FROM page WHERE name = ?), (SELECT "
			   "lastrevision+1 FROM page WHERE id = (SELECT id FROM page WHERE name = ?)));"
			<< revision.author << revision.comment << revision.content << revision.page << revision.page;
		*db << "UPDATE page SET lastrevision=lastrevision+1 WHERE name = ?; " << revision.page;
		*db << "release revisionsubmit;";
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
}

std::vector<Revision> RevisionDaoSqlite::getAllRevisions(QueryOption &options)
{
	std::vector<Revision> result;

	try
	{
		SqliteQueryOption queryOption{options};
		std::string queryOptionSql = queryOption.setPrependWhere(true)
										 .setListedColumnName("page.listed")
										 .setOrderByColumn("creationtime")
										 .build();
		auto query =
			*db
			<< "SELECT (SELECT username FROM user WHERE id = author), comment, content, strftime('%s',creationtime), "
			   "page.name, revisionid FROM revision INNER JOIN page ON revision.page = page.id " +
				   queryOptionSql;
		query >> [&](std::string author, std::string comment, std::string content, time_t creationtime,
					 std::string page, unsigned int revisionid)
		{
			Revision r;
			r.author = author;
			r.comment = comment;
			r.content = content;
			r.timestamp = creationtime;
			r.page = page;
			r.revision = revisionid;
			result.push_back(r);
		};
	}
	catch(const sqlite::errors::no_rows &e)
	{
		return result;
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return result;
}

std::vector<Revision> RevisionDaoSqlite::getAllRevisionsForPage(std::string pagename, QueryOption &option)
{
	std::vector<Revision> result;

	try
	{
		SqliteQueryOption queryOption{option};
		std::string queryOptionSql = queryOption.setPrependWhere(false)
										 .setListedColumnName("page.listed")
										 .setOrderByColumn("creationtime")
										 .build();
		auto query = *db << "SELECT (SELECT username FROM user WHERE id = author), comment, content, "
							"strftime('%s',creationtime), page.name, revisionid FROM revision INNER JOIN page ON "
							"revision.page = page.id WHERE page.name = ? AND " +
								queryOptionSql
						 << pagename;

		query >> [&](std::string author, std::string comment, std::string content, time_t creationtime,
					 std::string page, unsigned int revisionid)
		{
			Revision r;
			r.author = author;
			r.comment = comment;
			r.content = content;
			r.timestamp = creationtime;
			r.page = page;
			r.revision = revisionid;
			result.push_back(r);
		};
	}
	catch(const sqlite::errors::no_rows &e)
	{
		return result;
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return result;
}

std::optional<Revision> RevisionDaoSqlite::getCurrentForPage(std::string pagename)
{
	Revision result;
	try
	{
		auto query = *db << "SELECT (SELECT username FROM user WHERE id = author), comment, content, "
							"strftime('%s',creationtime), page.name, revisionid FROM revision INNER JOIN page ON "
							"revision.page = page.id WHERE page.name = ? AND page.lastrevision = revision.revisionid";
		query << pagename;
		query >>
			std::tie(result.author, result.comment, result.content, result.timestamp, result.page, result.revision);
	}
	catch(const sqlite::errors::no_rows &e)
	{
		return {};
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return result;
}

std::optional<Revision> RevisionDaoSqlite::getRevisionForPage(std::string pagename, unsigned int revision)
{
	Revision result;

	try
	{
		auto query =
			*db
			<< "SELECT (SELECT username FROM user WHERE id = author), comment, content, strftime('%s',creationtime), "
			   "page.name, revisionid FROM revision INNER JOIN page ON revision.page = page.id WHERE page.name = ? AND "
			   "revisionid = ?  ";
		query << pagename << revision;
		query >>
			std::tie(result.author, result.comment, result.content, result.timestamp, result.page, result.revision);
	}
	catch(const sqlite::exceptions::no_rows &e)
	{
		return {};
	}
	return result;
}

unsigned int RevisionDaoSqlite::countTotalRevisions()
{
	auto query = *db << "SELECT COUNT(ROWID) FROM revision";
	return static_cast<unsigned int>(execInt(query));
}

unsigned int RevisionDaoSqlite::countTotalRevisions(std::string page)
{
	auto query = *db << "SELECT COUNT(ROWID) FROM revision WHERE page = (SELECT id FROM page WHERE name = ?)" << page;
	return static_cast<unsigned int>(execInt(query));
}
