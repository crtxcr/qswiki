/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "permissionsdaosqlite.h"

PermissionsDaoSqlite::PermissionsDaoSqlite()
{
}

std::optional<Permissions> PermissionsDaoSqlite::find(std::string pagename, std::string username)
{
	auto query = *db << "SELECT permissions FROM permissions WHERE page = (SELECT id FROM page WHERE name = ?) AND "
						"userid = (SELECT id FROM user WHERE username = ?)";
	query << pagename << username;
	int permissions = 0;
	try
	{
		permissions = execInt(query);
	}
	catch(const sqlite::errors::no_rows &e)
	{
		return {};
	}

	return Permissions{permissions};
}

void PermissionsDaoSqlite::save(std::string pagename, std::string username, Permissions perms)
{
	try
	{
		auto query =
			*db
			<< "INSERT OR REPLACE INTO permissions (id, permissions, userid, page) VALUES((SELECT id FROM permissions "
			   "WHERE page = (SELECT id FROM page WHERE name = ?) AND userid = (SELECT id FROM user WHERE username = "
			   "?)), ?, (SELECT id FROM user WHERE username = ?), (SELECT id FROM page WHERE name = ?))";
		query << pagename << username << perms.getPermissions() << username << pagename;
		query.execute();
	}
	catch(const sqlite::errors::no_rows &e)
	{
		throwFrom(e);
	}
}

void PermissionsDaoSqlite::clearForPage(std::string pagename)
{
	try
	{
		auto stmt = *db << "DELETE FROM permissions WHERE page = (SELECT id FROM page WHERE name = ?)" << pagename;
		stmt.execute();
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
}
