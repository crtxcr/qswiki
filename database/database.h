#ifndef DATABASE_H
#define DATABASE_H
#include <memory>
#include <string>
#include "../user.h"
#include "../request.h"
#include "../response.h"
#include "pagedao.h"
#include "revisiondao.h"
#include "sessiondao.h"
#include "userdao.h"
#include "categorydao.h"
#include "permissionsdao.h"
class Database
{
  protected:
	std::string connnectionstring;

  public:
	Database()
	{
	}
	Database(std::string connstring)
	{
		this->connnectionstring = connstring;
	}

	virtual void beginTransaction() = 0;
	virtual void rollbackTransaction() = 0;
	virtual void commitTransaction() = 0;
	virtual std::unique_ptr<PageDao> createPageDao() const = 0;
	virtual std::unique_ptr<RevisionDao> createRevisionDao() const = 0;
	virtual std::unique_ptr<SessionDao> createSessionDao() const = 0;
	virtual std::unique_ptr<UserDao> createUserDao() const = 0;
	virtual std::unique_ptr<CategoryDao> createCategoryDao() const = 0;
	virtual std::unique_ptr<PermissionsDao> createPermissionsDao() const = 0;
	virtual ~Database()
	{
	}
};

#endif
