/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "sessiondaosqlite.h"
#include "userdaosqlite.h"

void SessionDaoSqlite::save(const Session &session)
{
	try
	{
		// TODO: we do not store creationtime
		auto q = *db << "INSERT OR REPLACE INTO session(id, token, csrf_token, creationtime, userid) VALUES((SELECT id "
						"FROM session WHERE token = ?), ?, ?, DATETIME(), (SELECT id FROM user WHERE username = ?))";
		q << session.token << session.token << session.csrf_token << session.user.login;
		q.execute();
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
}

void SessionDaoSqlite::deleteSession(std::string token)
{
	try
	{
		auto stmt = *db << "DELETE FROM session WHERE token = ?" << token;
		stmt.execute();
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
}

void SessionDaoSqlite::fillSession(int userid, Session &sess)
{
	if(userid > -1)
	{
		UserDaoSqlite userDao{*this->db};
		auto u = userDao.find(userid);
		if(u)
		{
			sess.user = *u;
		}
		else
		{
			Logger::error() << "Session for non existent user";
			throw DatabaseQueryException("Session for non existent user");
		}
	}
	else
	{
		sess.user = User::Anonymous();
	}
	sess.loggedIn = userid != -1;
}

std::optional<Session> SessionDaoSqlite::find(std::string token)
{
	Session result;

	try
	{
		std::string username;
		auto q = *db << "SELECT userid, token, csrf_token, strftime('%s', creationtime) FROM session WHERE token = ?"
					 << token;
		int userid;
		q >> std::tie(userid, result.token, result.csrf_token, result.creation_time);

		fillSession(userid, result);
	}
	catch(const sqlite::exceptions::no_rows &e)
	{
		return {};
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return result;
}

std::vector<Session> SessionDaoSqlite::fetch()
{
	std::vector<Session> result;

	*db << "SELECT userid, token, csrf_token, strftime('%s', creationtime) FROM session" >>
		[this, &result](int userid, std::string token, std::string csrf_token, time_t creationtime)
	{
		Session tmp;
		tmp.csrf_token = csrf_token;
		tmp.token = token;
		tmp.creation_time = creationtime;

		fillSession(userid, tmp);

		result.push_back(tmp);
	};

	return result;
}
