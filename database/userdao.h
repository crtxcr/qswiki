#ifndef USERDAO_H
#define USERDAO_H
#include <string>
#include <optional>
#include "../user.h"
#include "queryoption.h"
class UserDao
{
  public:
	UserDao();
	virtual bool exists(std::string username) = 0;
	virtual std::optional<User> find(std::string username) = 0;
	virtual std::optional<User> find(int id) = 0;
	virtual std::vector<User> list(QueryOption o) = 0;
	virtual void deleteUser(std::string username) = 0;
	virtual void save(const User &u) = 0;
	virtual ~UserDao(){};
};

#endif // USERDAO_H
