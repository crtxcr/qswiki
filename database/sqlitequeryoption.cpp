/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "sqlitequeryoption.h"

SqliteQueryOption::SqliteQueryOption(const QueryOption &o)
{
	this->o = o;
}

SqliteQueryOption &SqliteQueryOption::setOrderByColumn(std::string name)
{
	this->orderByColumnName = name;
	return *this;
}

SqliteQueryOption &SqliteQueryOption::setListedColumnName(std::string name)
{
	this->listedColumnName = name;
	return *this;
}

SqliteQueryOption &SqliteQueryOption::setPrependWhere(bool b)
{
	this->prependWhere = b;
	return *this;
}

std::string SqliteQueryOption::build()
{
	std::string result;
	if(this->prependWhere)
	{
		result += "WHERE ";
	}
	if(!o.includeUnlisted && !this->listedColumnName.empty())
	{
		result += this->listedColumnName + " = 1";
	}
	else
	{
		result += " 1 = 1";
	}
	result += " ORDER BY " + orderByColumnName;
	if(o.order == ASCENDING)
	{
		result += " ASC";
	}
	else
	{
		result += " DESC";
	}
	// TODO: limits for offset?
	if(o.limit > 0)
	{
		result += " LIMIT " + std::to_string(o.limit) + " OFFSET " + std::to_string(o.offset);
	}
	return result;
}
