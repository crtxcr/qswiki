/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <sqlite_modern_cpp/errors.h>
#include "pagedaosqlite.h"
#include "exceptions.h"
#include "sqlitequeryoption.h"
#include "../logger.h"
#include "../utils.h"

/* TODO: copied from C version mostly, review whether access to table other than page is ok */

bool PageDaoSqlite::exists(unsigned int id) const
{
	auto binder = *db << "SELECT 1 from page WHERE id = ?" << id;
	return execBool(binder);
}

bool PageDaoSqlite::exists(std::string name) const
{
	auto binder = *db << "SELECT 1 FROM page WHERE name = ?" << name;
	return execBool(binder);
}

std::optional<Page> PageDaoSqlite::find(std::string name)
{
	try
	{
		int pageid = fetchPageId(name);
		return find(pageid);
	}
	catch(const sqlite::errors::no_rows &e)
	{
		return {};
	}
}

std::optional<Page> PageDaoSqlite::findByTitle(std::string title)
{
	Page result;
	try
	{
		auto ps =
			*db
			<< "SELECT id, name, title, lastrevision, listed, feedlisted, (SELECT name FROM page WHERE id = parent) "
			   "FROM page WHERE title = ?";
		ps << title >> std::tie(result.pageid, result.name, result.title, result.current_revision, result.listed,
								result.feedlisted, result.parentpage);
	}
	catch(const sqlite::errors::no_rows &e)
	{
		return {};
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}

	return result;
}

std::optional<Page> PageDaoSqlite::find(unsigned int id)
{
	Page result;
	result.pageid = id;
	try
	{
		auto ps =
			*db
			<< "SELECT name, title, lastrevision, listed, feedlisted, (SELECT name FROM page WHERE id = parent) FROM "
			   "page WHERE id = ?";

		ps << id >> std::tie(result.name, result.title, result.current_revision, result.listed, result.feedlisted,
							 result.parentpage);
	}
	catch(const sqlite::errors::no_rows &e)
	{
		return {};
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}

	return result;
}

void PageDaoSqlite::deletePage(std::string page)
{
	int pageId = this->fetchPageId(page);
	// TODO on delete cascade is better most certainly
	try
	{
		*db << "BEGIN;";
		*db << "DELETE FROM revision WHERE page = ?;" << pageId;
		*db << "DELETE FROM categorymember WHERE page = ?;" << pageId;
		*db << "DELETE FROM permissions WHERE page = ?;" << pageId;
		*db << "DELETE FROM page WHERE id =?;" << pageId;
		*db << "COMMIT;";
	}
	catch(sqlite::sqlite_exception &e)
	{
		*db << "ROLLBACK";
		throwFrom(e);
	}
}

void PageDaoSqlite::save(const Page &page)
{
	try
	{
		*db << "INSERT OR REPLACE INTO page (id, name, title, lastrevision, listed, feedlisted, parent) VALUES((SELECT "
			   "id FROM page WHERE name = ? OR id = ?), ?, ?, ?, ?, ?, (SELECT id FROM page WHERE name = ?))"
			<< page.name << page.pageid << page.name << page.title << page.current_revision << page.listed
			<< page.feedlisted << page.parentpage;
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
}

std::vector<Page> PageDaoSqlite::getPageList(QueryOption option)
{
	std::vector<Page> result;

	try
	{
		std::string queryOption = SqliteQueryOption(option)
									  .setOrderByColumn("lower(name)")
									  .setListedColumnName("listed")
									  .setPrependWhere(true)
									  .build();
		std::string query = "SELECT id, name, title, lastrevision, listed, feedlisted,  (SELECT name FROM page WHERE "
							"id = parent) FROM page " +
							queryOption;
		*db << query >> [&](unsigned int pageid, std::string name, std::string title, unsigned int current_revision,
							bool listed, bool feedlisted, std::string parent)
		{
			Page tmp;
			tmp.pageid = pageid;
			tmp.name = name;
			tmp.title = title;
			tmp.current_revision = current_revision;
			tmp.listed = listed;
			tmp.feedlisted = feedlisted;
			tmp.parentpage = parent;
			result.push_back(tmp);
		};
	}
	catch(const sqlite::errors::no_rows &e)
	{
		return result;
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return result;
}

std::vector<std::string> PageDaoSqlite::fetchCategories(std::string pagename, QueryOption option)
{
	std::vector<std::string> result;
	try
	{
		auto query = *db << "SELECT name FROM categorymember INNNER JOIN category ON category = category.id WHERE page "
							"= (SELECT id FROM page WHERE name = ?)"
						 << pagename;
		query << " AND " << SqliteQueryOption(option).setPrependWhere(false).setOrderByColumn("name").build();
		query >> [&](std::string pagename) { result.push_back(pagename); };
	}
	catch(const sqlite::exceptions::no_rows &e)
	{
		return result;
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}

	return result;
}

std::string PageDaoSqlite::ftsEscape(std::string input)
{
	std::string result = "";
	for(auto &str : utils::split(input, ' '))
	{
		std::string tmp = utils::strreplace(str, "\"", "\"\"");
		tmp = "\"" + tmp + "\"" + " ";
		result += tmp;
	}
	if(!result.empty())
	{
		result.pop_back();
	}
	return result;
}

std::vector<SearchResult> PageDaoSqlite::search(std::string name, QueryOption option)
{

	std::vector<SearchResult> result;
	try
	{
		std::string qo = SqliteQueryOption(option).setPrependWhere(false).setOrderByColumn("rank").build();

		auto query =
			*db << "SELECT page.name FROM search INNER JOIN page ON search.page = page.id WHERE search MATCH ? "
				<< ftsEscape(name);
		query >> [&](std::string pagename)
		{
			SearchResult sresult;
			sresult.pagename = pagename;
			sresult.query = name;
			result.push_back(sresult);
		};
	}
	catch(const sqlite::exceptions::no_rows &e)
	{
		return result;
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return result;
}

void PageDaoSqlite::setCategories(std::string pagename, const std::vector<std::string> &catnames)
{
	try
	{
		int pageid = fetchPageId(pagename);
		*db << "savepoint setcategories;";
		*db << "DELETE FROM categorymember WHERE page = ?" << pageid;
		for(const std::string &cat : catnames)
		{
			*db << "INSERT OR IGNORE INTO category (id, name) VALUES( (SELECT id FROM category WHERE lower(name) = "
				   "lower(?)), lower(?))"
				<< cat << cat;
			*db << "INSERT INTO categorymember (category, page) VALUES ( (SELECT ID FROM category WHERE lower(name) = "
				   "lower(?)), ?)"
				<< cat << pageid;
		}
		*db << "release setcategories;";
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
}

int PageDaoSqlite::fetchPageId(std::string pagename)
{
	auto binder = *db << "SELECT id FROM page WHERE name = ?" << pagename;
	return execInt(binder);
}

std::vector<std::string> PageDaoSqlite::getChildren(std::string pagename)
{
	std::vector<std::string> result;
	auto query = *db << "SELECT name FROM page WHERE parent = (SELECT id FROM page WHERE name = ?)" << pagename;
	query >> [&](std::string page) { result.push_back(page); };
	return result;
}
