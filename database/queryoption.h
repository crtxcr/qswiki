#ifndef QUERYOPTION_H
#define QUERYOPTION_H

enum SORT_ORDER
{
	ASCENDING = 0,
	DESCENDING
};

class QueryOption
{
  public:
	unsigned int offset = 0;
	unsigned int limit = 0;
	SORT_ORDER order = ASCENDING;
	bool includeUnlisted = true;
};

#endif // QUERYOPTION_H
