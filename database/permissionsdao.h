#ifndef PERMISSIONSDAO_H
#define PERMISSIONSDAO_H
#include <optional>
#include "../permissions.h"
#include "../user.h"
class PermissionsDao
{
  public:
	PermissionsDao();
	virtual std::optional<Permissions> find(std::string pagename, std::string username) = 0;
	virtual void save(std::string pagename, std::string username, Permissions perms) = 0;
	virtual void clearForPage(std::string pagename) = 0;

	virtual ~PermissionsDao() = default;
};

#endif // PERMISSIONSDAO_H
