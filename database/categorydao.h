#ifndef CATEGORYDAO_H
#define CATEGORYDAO_H
#include <string>
#include <vector>
#include <optional>
#include "queryoption.h"
#include "../category.h"
#include "../page.h"
class CategoryDao
{
  public:
	CategoryDao();
	virtual void save(const Category &c) = 0;
	virtual std::vector<std::string> fetchList(QueryOption o) = 0;
	virtual std::optional<Category> find(std::string name) = 0;
	virtual void deleteCategory(std::string name) = 0;
	virtual std::vector<Page> fetchMembers(std::string name, QueryOption o) = 0;
	virtual ~CategoryDao() = default;
};

#endif // CATEGORYDAO_H
