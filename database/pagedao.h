#ifndef PAGEDAO_H
#define PAGEDAO_H
#include <string>
#include <optional>
#include <vector>
#include "queryoption.h"
#include "../page.h"
#include "../searchresult.h"
class PageDao
{
  public:
	PageDao();
	virtual bool exists(std::string page) const = 0;
	virtual bool exists(unsigned int id) const = 0;
	virtual std::optional<Page> find(std::string name) = 0;
	virtual std::optional<Page> findByTitle(std::string title) = 0;
	virtual std::optional<Page> find(unsigned int id) = 0;
	virtual std::vector<Page> getPageList(QueryOption option) = 0;
	virtual std::vector<std::string> fetchCategories(std::string pagename, QueryOption option) = 0;
	virtual void deletePage(std::string page) = 0;
	virtual void save(const Page &page) = 0;
	// TODO: this may not be the correct place for this.
	virtual void setCategories(std::string pagename, const std::vector<std::string> &catnames) = 0;
	virtual std::vector<SearchResult> search(std::string query, QueryOption option) = 0;

	virtual std::vector<std::string> getChildren(std::string pagename) = 0;

	virtual ~PageDao()
	{
	}
};

#endif // PAGEDAO_H
