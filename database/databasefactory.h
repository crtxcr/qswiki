#ifndef DATABASEFACTORY_H
#define DATABASEFACTORY_H
#include "../config.h"
#include "database.h"

std::unique_ptr<Database> createDatabase(const Config &o);
#endif // DATABASEFACTORY_H
