#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H
#include <stdexcept>

class DatabaseException : public std::runtime_error
{
	using std::runtime_error::runtime_error;
};

class DatabaseQueryException : public DatabaseException
{
	using DatabaseException::DatabaseException;
};

#endif // EXCEPTIONS_H
