#ifndef PERMISSIONSDAOSQLITE_H
#define PERMISSIONSDAOSQLITE_H
#include "permissionsdao.h"
#include "sqlitedao.h"

class PermissionsDaoSqlite : public PermissionsDao, protected SqliteDao
{
  public:
	PermissionsDaoSqlite();

	std::optional<Permissions> find(std::string pagename, std::string username) override;
	virtual void save(std::string pagename, std::string username, Permissions perms) override;
	virtual void clearForPage(std::string pagename) override;
	using SqliteDao::SqliteDao;
};

#endif // PERMISSIONSDAOSQLITE_H
