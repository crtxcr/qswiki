#ifndef PAGEDAOSQLITE_H
#define PAGEDAOSQLITE_H
#include <string>
#include <optional>
#include <sqlite3.h>
#include "../page.h"
#include "pagedao.h"
#include "sqlitedao.h"
class PageDaoSqlite : public PageDao, protected SqliteDao
{
  private:
	std::string ftsEscape(std::string input);

  public:
	PageDaoSqlite()
	{
	}
	void deletePage(std::string page) override;
	bool exists(unsigned int id) const override;
	bool exists(std::string name) const override;
	void save(const Page &page) override;
	std::optional<Page> find(std::string name) override;
	std::optional<Page> findByTitle(std::string title) override;
	std::optional<Page> find(unsigned int id) override;
	std::vector<Page> getPageList(QueryOption option) override;
	std::vector<std::string> fetchCategories(std::string pagename, QueryOption option) override;
	using SqliteDao::SqliteDao;
	int fetchPageId(std::string pagename);
	std::vector<SearchResult> search(std::string query, QueryOption option) override;
	void setCategories(std::string pagename, const std::vector<std::string> &catnames) override;
	std::vector<std::string> getChildren(std::string pagename) override;

};

#endif // PAGEDAOSQLITE_H
