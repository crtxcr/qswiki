/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "sqlitedao.h"

bool SqliteDao::execBool(sqlite::database_binder &binder) const
{
	bool result = false;
	try
	{
		binder >> result;
	}
	catch(sqlite::sqlite_exception &e)
	{
		// TODO: well, we may want to check whether rows have found or not and thus log this here
		result = false;
	}
	return result;
}

int SqliteDao::execInt(sqlite::database_binder &binder) const
{
	try
	{
		int result;
		binder >> result;
		return result;
	}
	catch(sqlite::exceptions::no_rows &e)
	{
		throw;
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return 0;
}
