#ifndef REVISIONDAOSQLITE_H
#define REVISIONDAOSQLITE_H
#include <sqlite3.h>
#include "revisiondao.h"
#include "sqlitedao.h"

class RevisionDaoSqlite : public RevisionDao, protected SqliteDao
{
  public:
	RevisionDaoSqlite();
	void save(const Revision &revision) override;
	std::vector<Revision> getAllRevisions(QueryOption &options) override;
	std::vector<Revision> getAllRevisionsForPage(std::string pagename, QueryOption &option) override;
	std::optional<Revision> getCurrentForPage(std::string pagename) override;
	std::optional<Revision> getRevisionForPage(std::string pagnename, unsigned int revision) override;
	unsigned int countTotalRevisions() override;
	unsigned int countTotalRevisions(std::string pagename) override;
	using SqliteDao::SqliteDao;
};

#endif // REVISIONDAOSQLITE_H
