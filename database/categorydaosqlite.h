#ifndef CATEGORYDAOSQLITE_H
#define CATEGORYDAOSQLITE_H

#include "categorydao.h"
#include "sqlitedao.h"
#include "../page.h"
class CategoryDaoSqlite : public CategoryDao, protected SqliteDao
{
  public:
	CategoryDaoSqlite();
	std::vector<std::string> fetchList(QueryOption o) override;
	std::vector<Page> fetchMembers(std::string name, QueryOption o) override;
	void save(const Category &c) override;
	void deleteCategory(std::string name) override;
	std::optional<Category> find(std::string name) override;
	using SqliteDao::SqliteDao;
};

#endif // CATEGORYDAOSQLITE_H
