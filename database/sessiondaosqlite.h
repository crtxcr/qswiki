#ifndef SESSIONDAOSQLITE_H
#define SESSIONDAOSQLITE_H
#include "sessiondao.h"
#include "../session.h"
#include "sqlitedao.h"

class SessionDaoSqlite : public SessionDao, protected SqliteDao
{
private:
  void fillSession(int userid, Session &sess);

  public:
	SessionDaoSqlite();
	void save(const Session &session) override;
	std::optional<Session> find(std::string token) override;
	void deleteSession(std::string token) override;
	std::vector<Session> fetch() override;
	using SqliteDao::SqliteDao;
};

#endif // SESSIONDAOSQLITE_H
