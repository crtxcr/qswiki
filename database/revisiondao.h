#ifndef REVISIONDAO_H
#define REVISIONDAO_H
#include <vector>
#include <optional>
#include "../revision.h"
#include "queryoption.h"
class RevisionDao
{
  public:
	virtual void save(const Revision &revision) = 0;
	virtual std::vector<Revision> getAllRevisions(QueryOption &options) = 0;
	virtual std::vector<Revision> getAllRevisionsForPage(std::string pagename, QueryOption &option) = 0;
	virtual std::optional<Revision> getCurrentForPage(std::string pagename) = 0;
	virtual std::optional<Revision> getRevisionForPage(std::string pagnename, unsigned int revision) = 0;
	virtual unsigned int countTotalRevisions() = 0;
	virtual unsigned int countTotalRevisions(std::string pagename) = 0;

	virtual ~RevisionDao()
	{
	}
};

#endif // REVISIONDAO_H
