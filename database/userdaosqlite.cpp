/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <sqlite3.h>
#include <string>
#include <memory>
#include <cstring>
#include "userdaosqlite.h"
#include "sqlitequeryoption.h"

UserDaoSqlite::UserDaoSqlite()
{
}

bool UserDaoSqlite::exists(std::string username)
{
	auto prep = *db << "SELECT 1 FROM user WHERE username = ?" << username;
	return execBool(prep);
}

std::optional<User> UserDaoSqlite::find(std::string username)
{
	try
	{
		User user;
		auto stmt = *db << "SELECT username, password, salt, permissions, enabled FROM user WHERE username = ?"
						<< username;

		int perms = 0;
		stmt >> std::tie(user.login, user.password, user.salt, perms, user.enabled);
		user.permissions = Permissions{perms};

		return user;
	}
	catch(const sqlite::errors::no_rows &e)
	{
		return {};
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return {};
}

std::optional<User> UserDaoSqlite::find(int id)
{
	try
	{
		User user;
		auto stmt = *db << "SELECT username, password, salt, permissions, enabled FROM user WHERE id = ?" << id;

		int perms = 0;
		stmt >> std::tie(user.login, user.password, user.salt, perms, user.enabled);
		user.permissions = Permissions{perms};

		return user;
	}
	catch(const sqlite::errors::no_rows &e)
	{
		return {};
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return {};
}

std::vector<User> UserDaoSqlite::list(QueryOption o)
{
	std::vector<User> result;

	try
	{

		std::string queryOption = SqliteQueryOption(o).setOrderByColumn("username").setPrependWhere(true).build();
		std::string query = "SELECT username, password, salt, permissions, enabled FROM user " + queryOption;

		*db << query >>
			[&](std::string username, std::vector<char> pw, std::vector<char> salt, int permisisons, bool enabled)
		{
			User tmp;
			tmp.login = username;
			tmp.password = pw;
			tmp.salt = salt;
			tmp.permissions = Permissions{permisisons};
			tmp.enabled = enabled;
			result.push_back(tmp);
		};
	}
	catch(const sqlite::errors::no_rows &e)
	{
		return result;
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
	return result;
}

void UserDaoSqlite::deleteUser([[maybe_unused]] std::string username)
{
	// What to do with the contributions of the user?
}

void UserDaoSqlite::save(const User &u)
{
	try
	{
		auto q = *db << "INSERT OR REPLACE INTO user(id, username, password, salt, permissions, enabled) "
						"VALUES((SELECT id FROM user WHERE username = ?), ?,?,?,?,?)";
		q << u.login << u.login << u.password << u.salt << u.permissions.getPermissions() << u.enabled;
		q.execute();
	}
	catch(sqlite::sqlite_exception &e)
	{
		throwFrom(e);
	}
}
