#ifndef SQLITEDAO_H
#define SQLITEDAO_H
#include <string_view>
#include <sqlite3.h>
#include <stdarg.h>
#include <sqlite_modern_cpp.h>
#include <sqlite_modern_cpp/errors.h>
#include "queryoption.h"
#include "exceptions.h"
#include "../logger.h"

class SqliteDao
{
  protected:
	sqlite::database *db = nullptr;

  public:
	SqliteDao()
	{
	}

	SqliteDao(sqlite::database &db)
	{
		this->db = &db;
	}
	void setDb(sqlite::database &db)
	{
		this->db = &db;
	}

	inline void throwFrom(const sqlite::sqlite_exception &e) const
	{
		std::string msg = "Sqlite Error: " + std::to_string(e.get_code()) + " SQL: " + e.get_sql();
		Logger::error() << msg << " Extended code: " << e.get_extended_code();
		throw DatabaseQueryException(msg);
	}

	bool execBool(sqlite::database_binder &binder) const;
	int execInt(sqlite::database_binder &binder) const;

	virtual ~SqliteDao() = default;
};

#endif // SQLITEDAO_H
