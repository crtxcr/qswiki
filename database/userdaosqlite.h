#ifndef USERDAOSQLITE_H
#define USERDAOSQLITE_H
#include <string>
#include <optional>
#include <vector>
#include "userdao.h"
#include "sqlitedao.h"

class UserDaoSqlite : public UserDao, protected SqliteDao
{
  public:
	bool exists(std::string username);
	std::optional<User> find(std::string username);
	std::optional<User> find(int id);

	std::vector<User> list(QueryOption o);
	void deleteUser(std::string username);
	void save(const User &u);
	using SqliteDao::SqliteDao;
	UserDaoSqlite();
};

#endif // USERDAOSQLITE_H
