#ifndef SESSIONDAO_H
#define SESSIONDAO_H
#include <string>
#include <optional>
#include "../session.h"
class SessionDao
{
  public:
	SessionDao();
	virtual void save(const Session &session) = 0;
	virtual std::optional<Session> find(std::string token) = 0;
	virtual void deleteSession(std::string token) = 0;
	virtual std::vector<Session> fetch() = 0;
	virtual ~SessionDao()
	{
	}
};

#endif // SESSIONDAO_H
