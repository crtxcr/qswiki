/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <atomic>
#include "sqlite.h"
#include "pagedaosqlite.h"
#include "revisiondaosqlite.h"
#include "sessiondaosqlite.h"
#include "sqlite_modern_cpp.h"
#include "userdaosqlite.h"
#include "categorydaosqlite.h"
#include "permissionsdaosqlite.h"

thread_local sqlite::database *Sqlite::db = nullptr;

std::atomic<int> instances = 0;
Sqlite::Sqlite(std::string path) : Database(path)
{
	instances++;
	if(instances.load() > 1)
	{
		std::cerr << "temporal (yeah, right) HACK... only one instance allowed" << std::endl;
		abort();
	}
}

std::mutex dbmutex;
sqlite::database &Sqlite::database() const
{
	if(Sqlite::db == nullptr)
	{
		sqlite::sqlite_config config;
		config.flags = config.flags | sqlite::OpenFlags::FULLMUTEX;
		std::lock_guard<std::mutex> dbguard(dbmutex);
		Sqlite::db = new sqlite::database(this->connnectionstring, config);
		*Sqlite::db << "PRAGMA journal_mode=WAL;";
		*Sqlite::db << "PRAGMA busy_timeout=10000;";
	}
	return *Sqlite::db;
}
std::unique_ptr<RevisionDao> Sqlite::createRevisionDao() const
{
	return create<RevisionDaoSqlite>();
}

std::unique_ptr<PageDao> Sqlite::createPageDao() const
{
	return create<PageDaoSqlite>();
}

std::unique_ptr<UserDao> Sqlite::createUserDao() const
{
	return create<UserDaoSqlite>();
}

std::unique_ptr<SessionDao> Sqlite::createSessionDao() const
{
	return create<SessionDaoSqlite>();
}

std::unique_ptr<CategoryDao> Sqlite::createCategoryDao() const
{
	return create<CategoryDaoSqlite>();
}

std::unique_ptr<PermissionsDao> Sqlite::createPermissionsDao() const
{
	return create<PermissionsDaoSqlite>();
}

void Sqlite::beginTransaction()
{
	*db << "begin;";
}

void Sqlite::rollbackTransaction()
{
	*db << "rollback;";
}

void Sqlite::commitTransaction()
{
	*db << "commit;";
}

Sqlite::~Sqlite()
{
	delete this->db;
}
