#ifndef SQLITE_H
#define SQLITE_H
#include <string>
#include <sqlite3.h>
#include <sqlite_modern_cpp.h>
#include "database.h"

class Sqlite : public Database
{
  private:
	static thread_local sqlite::database *db;

	template <class T> std::unique_ptr<T> create() const
	{
		return std::make_unique<T>(database());
	}

	sqlite::database &database() const;

  public:
	Sqlite(std::string path);
	std::unique_ptr<PageDao> createPageDao() const;
	std::unique_ptr<RevisionDao> createRevisionDao() const;
	std::unique_ptr<UserDao> createUserDao() const;
	std::unique_ptr<SessionDao> createSessionDao() const;
	std::unique_ptr<CategoryDao> createCategoryDao() const;
	std::unique_ptr<PermissionsDao> createPermissionsDao() const;
	void beginTransaction();
	void commitTransaction();
	void rollbackTransaction();
	virtual ~Sqlite();
};

#endif // SQLITE_H
