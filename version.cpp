#include "version.h"

std::string git_commit_id()
{
	return std::string(GITCOMMIT);
}

std::string get_version_string()
{
	return git_commit_id() + " Built: " + __DATE__ + " " + __TIME__;
}
