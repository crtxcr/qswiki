/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "cgi.h"
#include "../utils.h"
#include <cstdlib>
#include <string>
#include <memory>
#include <iostream>
Cgi::Cgi(const Config &c)
{
	this->config = &c;
}

bool Cgi::keepReading()
{
	return !this->responseSent;
}

Request Cgi::readRequest()
{

	std::string request_uri = utils::getenv("REQUEST_URI");
	if(request_uri == "")
	{
		throw std::runtime_error("REQUEST_URI is empty");
	}

	Request result{request_uri};

	std::string method = utils::getenv("REQUEST_METHOD");
	if(method == "POST")
	{
		std::string content_type = utils::getenv("CONTENT_TYPE");
		if(content_type != "application/x-www-form-urlencoded")
		{
			throw "invalid content_type";
		}
		std::string content_length = utils::getenv("CONTENT_LENGTH");
		int cl = std::stoi(content_length);
		std::unique_ptr<char[]> ptr(new char[cl + 1]);
		std::cin.get(ptr.get(), cl + 1);

		std::string post_data{ptr.get()};
	}

	result.initCookies(utils::getenv("HTTP_COOKIE"));
	result.setIp(utils::getenv("REMOTE_ADDR"));
	result.setUseragent(utils::getenv("HTTP_USER_AGENT"));

	return result;
}

void Cgi::work(RequestWorker &worker)
{
	while(this->keepReading())
	{
		Request req = readRequest();
		sendResponse(worker.processRequest(req));
	}
}

void Cgi::sendResponse(const Response &r)
{
	std::cout << "Status: " << r.getStatus() << "\r\n";
	std::cout << "Content-Type: " << r.getContentType() << "\r\n";
	for(auto header : r.getResponseHeaders())
	{
		std::string key = header.first;
		std::string second = header.second;
		if(key.back() != ':')
		{
			std::cout << key << ":" << second << "\r\n";
		}
		else
		{
			std::cout << key << second << "\r\n";
		}
	}
	for(const Cookie &c : r.getCookies())
	{
		std::cout << "Set-Cookie: " << c.createHeaderValue() << "\r\n";
	}
	std::cout << "\r\n";
	std::cout << r.getBody();
	std::cout.flush();
	this->responseSent = true;
}

Cgi::~Cgi()
{
}
