#ifndef GATEWAYINTERFACE_H
#define GATEWAYINTERFACE_H
#include "../request.h"
#include "../response.h"
#include "../config.h"
#include "../requestworker.h"
class GatewayInterface
{
  public:
	GatewayInterface();
	virtual bool keepReading() = 0;
	virtual void work(RequestWorker &worker) = 0;
	virtual ~GatewayInterface()
	{
	}
};

#endif // GATEWAYINTERFACE_H
