/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "gatewayfactory.h"
#include "cgi.h"
#include "httpgateway.h"

std::unique_ptr<GatewayInterface> createGateway(const Config &c)
{
	std::string listenaddr = c.configVarResolver.getConfig("http.listenaddr");
	if(listenaddr.empty())
	{
		throw new std::runtime_error("No http.listenaddr in config file");
	}
	std::string listenport = c.configVarResolver.getConfig("http.listenport");
	if(listenport.empty())
	{
		throw new std::runtime_error("No http.listenport in config file");
	}

	return std::make_unique<HttpGateway>(listenaddr, std::stoi(listenport), c.max_payload_length);
}
