#ifndef GATEWAYFACTORY_H
#define GATEWAYFACTORY_H
#include <memory>
#include "../config.h"
#include "gatewayinterface.h"
std::unique_ptr<GatewayInterface> createGateway(const Config &c);

#endif // GATEWAYFACTORY_H
