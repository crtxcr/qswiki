#ifndef HTTPGATEWAY_H
#define HTTPGATEWAY_H

#define CPPHTTPLIB_FORM_URL_ENCODED_PAYLOAD_MAX_LENGTH 65536

#include <httplib.h>
#include "gatewayinterface.h"
#include "../requestworker.h"
#include "../request.h"
#include "../response.h"
#include "../utils.h"
class HttpGateway : public GatewayInterface
{
  private:
	Response convertResponse(httplib::Response response);
	httplib::Response convertResponse(Response response);
	Request convertRequest(httplib::Request request);
	//  void worker(const httplib::Request& req, httplib::Response& res);

	std::string listenaddr;
	int listenport;
	uint64_t maxPayloadLength;

  public:
	HttpGateway(std::string listenaddr, int port, uint64_t maxPayloadLength);
	bool keepReading() override;
	void work(RequestWorker &worker) override;
};

#endif // HTTPGATEWAY_H
