#ifndef CGI_H
#define CGI_H

#include "gatewayinterface.h"
#include "../requestworker.h"
class Cgi : public GatewayInterface
{
  private:
	bool responseSent = false;
	const Config *config;
	Request readRequest();
	void sendResponse(const Response &r);

  public:
	Cgi(const Config &c);
	bool keepReading() override;
	void work(RequestWorker &worker) override;

	~Cgi();
};

#endif // CGI_H
