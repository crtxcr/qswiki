#ifndef USER_H
#define USER_H
#include <utility>
#include <vector>
#include "permissions.h"
class User
{
  private:
	static User anonUser;

  public:
	static const User &Anonymous();
	static void setAnon(User u)
	{
		User::anonUser = std::move(u);
	}
	std::string login;
	std::vector<char> password;
	std::vector<char> salt;
	bool enabled;
	Permissions permissions;
	User();
};

#endif
