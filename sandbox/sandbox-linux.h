#ifndef SANDBOXLINUX_H
#define SANDBOXLINUX_H
#include <memory>
#include <vector>
#include "sandbox.h"
class SandboxLinux : public Sandbox
{
  public:
	using Sandbox::Sandbox;
	bool supported() override;
	bool enable(std::vector<std::string> fsPaths) override;
};
#endif
