#ifndef SANDBOXOPENBSD_H
#define SANDBOXOPENBSD_H
#include "sandbox.h"

class SandboxOpenBSD : public Sandbox
{
  public:
	bool supported() override;
	bool enable(std::vector<std::string> fsPaths) override;
};
#endif
