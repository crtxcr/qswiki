#ifndef SANDBOX_H
#define SANDBOX_H
#include <vector>
class Sandbox
{
  public:
	Sandbox()
	{
	}
	/* Whether the platform has everything required to active all sandbnox modes */
	virtual bool supported() = 0;

	/* Activated after we have acquired resources (bound to ports etc.)*/
	virtual bool enable(std::vector<std::string> fsPaths) = 0;
};
#endif
