#ifndef SANDBOXFACTORY_H
#define SANDBOXFACTORY_H
#include <memory>
#include "sandbox.h"
#include "sandbox-linux.h"
#include "sandbox-openbsd.h"
inline std::unique_ptr<Sandbox> createSandbox()
{
	return std::make_unique<SandboxLinux>();
}
#endif
