/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "requestworker.h"
#include "handlers/handlerfactory.h"

Session RequestWorker::retrieveSession(std::string token) const
{
	if(token.empty())
	{
		return Session::createAnon();
	}

	auto sess = this->sessionDao->find(token);
	if(sess)
	{
		sess->creation_time = time(0);
		return *sess;
	}
	else
	{
		return Session::createAnon();
	}
}

Response RequestWorker::processRequest(const Request &r)
{
	std::string sessiontoken = r.cookie("sessiontoken");
	Session session;
	if(sessiontoken != "")
	{
		session = retrieveSession(sessiontoken);
	}
	else
	{
		session = Session::createAnon();
	}

	if(r.getRequestMethod() == "POST")
	{
		// TODO: also protect non-logged in users (with a mechanism not involving cookies)
		if(session.loggedIn && session.csrf_token != r.post("csrf_token"))
		{
			// TODO: this is code duplication
			TemplatePage error = this->templ->getPage("error");
			error.setVar("errortitle", "Invalid csrf token");
			error.setVar("errormessage", "Invalid csrf token");
			return {403, error.render()};
		}
	}

	auto handler = handlerFactory->createHandler(r.param("action"), session);
	try
	{
		Response response = handler->handle(r);
		if(session.loggedIn)
		{
			Cookie sessionCookie{"sessiontoken", session.token};
			response.addCookie(sessionCookie);
			this->sessionDao->save(session);
		}
		return response;
	}
	catch(std::exception &e)
	{
		Logger::error() << "Exception catched by requestworker: " << e.what();
		Response response;
		response.setBody("General unknown error");
		response.setContentType("text/plain");
		return response;
	}
}
