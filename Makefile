CPPSTD=c++20

#CFIFLAGS=-fsanitize=cfi -fvisibility=hidden -fsanitize=cfi -flto
#Does not work reliably atm
CFIFLAGS=

CXXFLAGS=-std=$(CPPSTD) -O2 -g -no-pie -pipe -MMD -Wall -Wextra -DGLIBCXX_ASSERTIONS -D_LIBCPP_ENABLE_ASSERTIONS=1 $(CFIFLAGS)
RELEASE_CXXFLAGS=-std=$(CPPSTD) -O3 -pipe -MMD -Wall -Wextra -DGLIBCXX_ASSERTIONS -D_LIBCPP_ENABLE_ASSERTIONS=1 $(CFIFLAGS)

LDFLAGS=-lsqlite3 -lpthread -lcrypto -lstdc++fs $(CFIFLAGS)
INCLUDEFLAGS=-I submodules/sqlitemoderncpp/hdr -I submodules/cpp-httplib -I submodules/exile.h

SOURCES=$(wildcard *.cpp)
SOURCES+=$(wildcard gateway/*.cpp)
SOURCES+=$(wildcard handlers/*.cpp)
SOURCES+=$(wildcard database/*.cpp)
SOURCES+=$(wildcard cache/*.cpp)
SOURCES+=$(wildcard sandbox/*.cpp)
SOURCES+=$(wildcard dynamic/*.cpp)

HEADERS=$(wildcard *.h)
HEADERS+=$(wildcard gateway/*.h)
HEADERS+=$(wildcard handlers/*.h)
HEADERS+=$(wildcard database/*.h)
HEADERS+=$(wildcard cache/*.h)
HEADERS+=$(wildcard sandbox/*.h)
HEADERS+=$(wildcard dynamic/*.h)

OBJECTS=$(patsubst %.cpp, %.o, $(SOURCES))
WIKIOBJECTS=$(filter-out test.o, $(OBJECTS))
TESTOBJECTS=$(filter-out qswiki.o, $(OBJECTS))
DEPENDS = ${WIKIOBJECTS:.o=.d}
-include ${DEPENDS}

# Points to the root of Google Test, relative to where this file is.
# Remember to tweak this if you move this file.
GTEST_DIR = /home/data/SOURCES/gtest/googletest

GTESTS_TESTDIR = ./tests/

GTEST_CXXFLAGS=-std=$(CPPSTD) -isystem $(GTEST_DIR)/include -I$(GTEST_DIR) -g -O0 -pipe -Wall -Wextra
GTEST_LDFLAGS=-lsqlite3 -g -O0 -lpthread -lcrypto -lstdc++fs
GTEST_OBJECTS=$(filter-out qswiki.o, $(WIKIOBJECTS))

.DEFAULT_GOAL := qswiki

release: CXXFLAGS=$(RELEASE_CXXFLAGS)
profile: CXXFLAGS=$(RELEASE_CXXFLAGS) -pg
profile: LDFLAGS+= -pg

release: qswiki
profile: qswiki


exile.o: submodules/exile.h/exile.c
	$(CC) -std=c99 -DHAVE_LANDLOCK=0 -c submodules/exile.h/exile.c -o exile.o

qswiki: $(WIKIOBJECTS) exile.o
	$(CXX) $(shell shuf -e $(WIKIOBJECTS) exile.o ) ${LDFLAGS} ${INCLUDEFLAGS}  -o qswiki

test: $(TESTOBJECTS)
	$(CXX) $(TESTOBJECTS) ${LDFLAGS} -o test

gtest: $(GTESTS_TESTDIR)/*.cpp $(GTEST_OBJECTS)
	$(CXX) -o gtest $(GTESTS_TESTDIR)/*.cpp $(GTEST_OBJECTS) $(GTEST_CXXFLAGS) $(GTEST_DIR)/src/gtest_main.cc $(GTEST_DIR)/src/gtest-all.cc $(GTEST_LDFLAGS)

%.o:%.cpp
	$(CXX) ${CXXFLAGS} ${INCLUDEFLAGS} -c -o $@ $<

version.o:version.cpp
	$(CXX) ${CXXFLAGS} ${INCLUDEFLAGS} -DGITCOMMIT=\"$(shell git rev-parse --short HEAD)\" -c -o $@ $<
clean:
	rm -f exile.o $(OBJECTS) $(DEPENDS)


