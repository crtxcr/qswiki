#include "revisionrenderer.h"
#include "templatepage.h"
#include "dynamic/dynamiccontentpostlist.h"
#include "dynamic/dynamiccontentincludepage.h"
#include "dynamic/dynamiccontentgetvar.h"
#include "dynamic/dynamiccontentsetvar.h"
#include "dynamic/dynamicpostrenderer.h"
#include "parser.h"
#include "htmllink.h"

std::string RevisionRenderer::dynamicCallback(std::string_view key, std::string_view value)
{
	if(key == "dynamic:postlist")
	{
		auto postlist = this->dynamicContentFactory.createDynamicContent<DynamicContentPostList>();
		postlist->setArgument(std::string(value));
		return postlist->render();
	}
	if(key == "dynamic:includepage")
	{
		auto includePage = this->dynamicContentFactory.createDynamicContent<DynamicContentIncludePage>();
		includePage->setArgument(std::string(value));
		return parser.parseDynamics(includePage->render(), std::bind(&RevisionRenderer::dynamicCallback, this,
																	 std::placeholders::_1, std::placeholders::_2));
	}
	if(key == "dynamic:setvar")
	{
		auto setVar = this->dynamicContentFactory.createDynamicContent<DynamicContentSetVar>();
		setVar->setMap(dynamicVarsMap);
		setVar->setArgument(std::string(value));
		return setVar->render();
	}
	if(key == "dynamic:getvar")
	{
		auto getVar = this->dynamicContentFactory.createDynamicContent<DynamicContentGetVar>();
		getVar->setMap(dynamicVarsMap);
		getVar->setArgument(std::string(value));
		return getVar->render();
	}
	if(key == "dynamic:postrenderer")
	{
		auto renderer = this->dynamicContentFactory.createDynamicContent<DynamicPostRenderer>();
		renderer->setArgument(std::string(value));
		return renderer->render();
	}
	return std::string{};
}

std::string RevisionRenderer::renderContent(std::string content)
{
	dynamicVarsMap["pagetitle"] = parser.extractCommand("pagetitle", content);
	dynamicVarsMap["createdon"] = utils::toISODate(time(NULL));
	dynamicVarsMap["modifydatetime"] = utils::toISODateTime(time(NULL));

	std::string resolvedContent = parser.parseDynamics(
		content, std::bind(&RevisionRenderer::dynamicCallback, this, std::placeholders::_1, std::placeholders::_2));

	return parser.parse(*this->db->createPageDao(), *this->urlProvider, resolvedContent);
}

std::string RevisionRenderer::renderContent(const Revision &r, std::string_view customTitle)
{
	auto revisionDao = this->db->createRevisionDao();
	auto firstRevision = revisionDao->getRevisionForPage(r.page, 1);
	if(!firstRevision)
	{
		throw std::runtime_error("Could not get first revision for page, which is odd. Solar flares?");
	}

	dynamicVarsMap["createdon"] = utils::toISODate(firstRevision.value().timestamp);
	dynamicVarsMap["pagetitle"] = customTitle;
	dynamicVarsMap["modifydatetime"] = utils::toISODateTime(r.timestamp);

	std::string resolvedContent = parser.parseDynamics(
		r.content, std::bind(&RevisionRenderer::dynamicCallback, this, std::placeholders::_1, std::placeholders::_2));

	return parser.parse(*this->db->createPageDao(), *this->urlProvider, resolvedContent);
}
