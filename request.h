#include <utility>

#include <utility>

#ifndef REQUEST_H
#define REQUEST_H
#include <string>
#include <map>
#include <utility>
#include <vector>
#include <ostream>
#include "cookie.h"
class Request
{
  private:
	std::multimap<std::string, std::string> getVars;
	std::multimap<std::string, std::string> postVars;

	std::string url;
	std::string ip;
	std::string useragent;
	std::vector<Cookie> cookies;
	std::string request_method;

	void initMultiMap(std::multimap<std::string, std::string> &map, const std::string &url);
	std::pair<std::string, std::string> createPairFromVar(std::string var);

  public:
	Request()
	{
	}
	Request(std::string url);
	std::string get(const std::string &key) const;
	std::string post(const std::string &key) const;
	std::string cookie(const std::string &key) const;
	std::string param(const std::string &key) const;
	std::vector<std::string> allGet(const std::string &key);
	std::vector<std::string> allPost(const std::string &key);

	const std::vector<Cookie> &getCookies() const
	{
		return this->cookies;
	}

	void setCookies(std::vector<Cookie> cookies)
	{
		this->cookies = std::move(cookies);
	}

	void setGetVars(std::multimap<std::string, std::string> getVars)
	{
		this->getVars = std::move(getVars);
	}

	void setPostVars(std::multimap<std::string, std::string> postVars)
	{
		this->postVars = std::move(postVars);
	}

	void setIp(const std::string &ip)
	{
		this->ip = ip;
	}

	void setUseragent(const std::string &agent)
	{
		this->useragent = agent;
	}

	void setUrl(const std::string &url)
	{
		this->url = url;
	}

	std::string getUrl() const
	{
		return url;
	}

	std::string getIp() const
	{
		return ip;
	}

	std::string getUseragent() const
	{
		return useragent;
	}

	std::string getRequestMethod() const
	{
		return request_method;
	}

	inline void setRequestMethod(std::string request_method)
	{
		this->request_method = request_method;
	}

	void initGetMap(const std::string &url);
	void initPostMap(const std::string &url);
	void initCookies(const std::string &cookiestr);

	friend std::ostream &operator<<(std::ostream &os, const Request &req);
};

inline std::ostream &operator<<(std::ostream &os, const Request &req)
{
	os << req.request_method << " " << req.url << " " << req.ip;
	return os;
}
#endif // REQUEST_H
