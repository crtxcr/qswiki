/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include <sstream>
#include <exception>
#include <memory>
#include <vector>
#include "random.h"
#include "logger.h"

Random::Random()
{
}

std::vector<char> Random::getRandom(unsigned int bytes) const
{
	auto buffer = std::make_unique<char[]>(bytes);
	int r = getrandom(buffer.get(), bytes, GRND_NONBLOCK);
	if(r != -1 &&  (size_t)r == bytes)
	{
		char *ptr = buffer.get();
		return { ptr, ptr + bytes };
	}
	else
	{
		Logger::error() << "Random generator failed to get bytes: " + std::to_string(r);
		throw std::runtime_error("Random generator failed");
	}

}

std::string Random::getRandomHexString(unsigned int bytes) const
{
	std::stringstream stream;

	std::vector<char> random = getRandom(bytes);
	for(size_t i = 0; i < random.size(); i++)
	{
		unsigned char c = (unsigned char)random[i];
		stream << std::hex << (unsigned int)c;
	}
	return stream.str();

}
