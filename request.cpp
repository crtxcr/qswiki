/* Copyright (c) 2018 Albert S.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#include "request.h"
#include "utils.h"
Request::Request(std::string url)
{
	this->url = url;
	size_t question = url.find_first_of('?');
	if(question != std::string::npos)
	{
		initGetMap(url.substr(question + 1));
	}
}

std::pair<std::string, std::string> Request::createPairFromVar(std::string var)
{
	size_t equal = var.find_first_of('=');
	if(equal == std::string::npos)
	{
		return std::make_pair(std::move(var), "");
	}
	else
	{
		std::string key = var.substr(0, equal);
		std::string val = utils::html_xss(utils::urldecode(var.substr(equal + 1)));
		return std::make_pair(std::move(key), std::move(val));
	}
}

void Request::initMultiMap(std::multimap<std::string, std::string> &map, const std::string &url)
{
	auto splitted = utils::split(url, '&');
	for(const std::string &part : splitted)
	{
		auto pair = createPairFromVar(part);
		map.insert(pair);
	}
}
void Request::initGetMap(const std::string &url)
{
	size_t question = url.find_first_of('?');
	if(question != std::string::npos)
	{
		initMultiMap(getVars, url.substr(question + 1));
	}
	else
	{
		initMultiMap(getVars, url);
	}
}

void Request::initPostMap(const std::string &url)
{
	initMultiMap(postVars, url);
}

void Request::initCookies(const std::string &cookiestr)
{
	// TODO: find out what it really should be, ";" or "; "?
	std::regex regex{";+\\s?"};
	auto cookiesplitted = utils::split(cookiestr, regex);
	for(const std::string &part : cookiesplitted)
	{
		auto pair = createPairFromVar(part);
		cookies.push_back(Cookie(pair.first, pair.second));
	}
}

std::string Request::get(const std::string &key) const
{
	return utils::getKeyOrEmpty(this->getVars, key);
}

std::string Request::post(const std::string &key) const
{
	return utils::getKeyOrEmpty(this->postVars, key);
}

std::string Request::param(const std::string &key) const
{
	std::string getvar = get(key);
	if(getvar.empty())
	{
		return post(key);
	}
	return getvar;
}
std::string Request::cookie(const std::string &key) const
{
	for(const Cookie &c : cookies)
	{
		if(c.key == key)
		{
			return c.value;
		}
	}

	return "";
}

std::vector<std::string> Request::allGet(const std::string &key)
{
	return utils::getAll(this->getVars, key);
}

std::vector<std::string> Request::allPost(const std::string &key)
{
	return utils::getAll(this->postVars, key);
}
