#ifndef HTMLLINK_H
#define HTMLLINK_H
#include <string>

class HtmlLink
{
  public:
	HtmlLink();
	std::string href;
	std::string innervalue;
	std::string cssclass;

	std::string render()
	{
		return "<a href=\"" + href + "\" class=\"" + cssclass + "\">" + innervalue + "</a>";
	}
};

#endif // HTMLLINK_H
